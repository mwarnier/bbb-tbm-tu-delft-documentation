#!/usr/bin/python3
import sys
import psycopg2
import time
import datetime
import hashlib
import docker
import sqlite3

######### !!!! SCRIPT MUST BE EXECUTED AS ROOT !!!! #########


# Change to anything else to execute on production instead of DEV
# Make sure to update the psql password in the connect string
envi="DEV"

# Change to anything else to irrecoverably remove users
# DEL just disables accounts. Can be chained, i.e., disable first, delete later
dst='DEL'


conn = psycopg2.connect("dbname=greenlight_production user=greenlight password=PASSWORD host=10.23.42.2")
if envi == "DEV":
	CONTAINER = 'greenlight-dev-v2'
else:
	CONTAINER = 'greenlight-v2'
client = docker.from_env()
container = client.containers.get(CONTAINER)

def print_percent(f_cur, f_max, str_file):
	f_cur = float(f_cur)
	f_max = float(f_max)

	f_per = (f_cur/f_max * 100)

	sys.stderr.flush()
	sys.stderr.write(str_file+" : [ "+"#"*int(f_per)+" "*(100-int(f_per-1))+"]  "+str(round(f_per,2))+"%\r")
	sys.stderr.flush()


def get_users():
	cur = conn.cursor()
	cur.execute("SELECT name,email FROM users where provider != 'openid_connect';")
		
	users = cur.fetchall()
	
	return users

def get_users_dev():
	conn = sqlite3.connect('/srv/greenlight-surf-dev/db/production/production.sqlite3')
	cur = conn.cursor()
	cur.execute("SELECT name,email FROM users where provider != 'openid_connect';")
		
	users = cur.fetchall()
	return users

def get_user_mails(users):
	r = []
	for u in users:
		r.append(u[1])
	return r

def read_exceptions(f='./exceptions'):
	e = []
	for l in open(f):
		e.append(l.strip())
	return e

def check_exceptions(users):
	print("Doing preflight check for allow list.")
	e = read_exceptions()
	u = get_user_mails(users)
	
	elen = len(e)
	missing = 0
	
	for i in e:
		if not i in u:
			print("WARNING: "+i+' not found in user database; Did you use the right email address for allow listing?')
			missing = missing + 1
	
	if missing == 0:
		print('Preflight OK: All allowlisted users found')
		return e
	else:
		print('ERROR: Allow list not complete! '+str(missing)+' user(s) not found!')
		sys.exit(0)

def get_delete_users(users, e):
	del_list = []
	for u in users:
		if not u[1] in e:
			del_list.append(u[1])
	return del_list

def delete_user(u):
	container.exec_run('bundle exec rails runner -e production "User.include_deleted.find_by(email: \''+u+'\').delete"')

def destroy_user(u):
	container.exec_run('bundle exec rails runner -e production "User.include_deleted.find_by(email: \''+u+'\').destroy(true)"')

# set to 20s before issuing 
timeout = 0

c = 0
if envi == "DEV":
	users = get_users_dev()
else:
	users = get_users()
lu = len(users)

e = check_exceptions(users)
users_to_delete = get_delete_users(users, e)
ldu = len(users_to_delete)

if not envi == "DEV":
	print("!!!! YOU ARE EXECUTING THIS CHANGE ON PRODUCTION !!!!")

if dst == 'DEL':
	print('Ready to delete '+str(ldu)+'/'+str(lu)+') users ('+str(len(e))+' allowlisted)')
	print('If you want to continue type "YES, DELETE '+str(ldu)+' USERS!" (without quotes) and then hit enter:')
	statement = input()
	
	if not statement == 'YES, DELETE '+str(ldu)+' USERS!':
		sys.exit(0)
else:
	print('Ready to >>>PERMANENTLY REMOVE<<< '+str(ldu)+'/'+str(lu)+') users ('+str(len(e))+' allowlisted)')
	print('If you want to continue type "YES, >>>PERMANENTLY REMOVE<<< '+str(ldu)+' USERS!" (without quotes) and then hit enter:')
	statement = input()
	
	if not statement == 'YES, >>>PERMANENTLY REMOVE<<< '+str(ldu)+' USERS!':
		sys.exit(0)


for i in users_to_delete:
	if dst == 'DEL':
		delete_user(i)
	else:
		destroy_user(i)
	
	c = c + 1
	print_percent(c, ldu, 'deleting users')
print_percent(c, ldu, 'deleting users')
print()
