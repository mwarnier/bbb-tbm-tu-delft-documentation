# bbb-tbm-tudelft-nl-documentation

## Infrastructure Information
- [Information on our hosters](infrastructure_provider/provider.md)
- [Change Log](changelog/changelog.md)

## Topology Overview
![alt text](hosts/topology/bbb-topology.png "Topology Overview")

## Administration Paradigms
- If the system fails in a way that is not yet being monitored, write/add a check for the root-cause
- If the system fails, identify the root-cause and ensure it does not occur again
- If you make changes, note them in the log (see above)
- The servers should be rebootable, but don't do it with active users to avoid service interruption
- Commit to etckeeper

## Host Documentation
- [Basic configuration for all hosts](hosts/base_config.md)

### mgmt.bbb.tbm.tudelft.nl
- [Host Basics](hosts/mgmt.bbb.tbm.tudelft.nl/host.md)
- [Updating](hosts/mgmt.bbb.tbm.tudelft.nl/updating.md)
- [Installation](hosts/mgmt.bbb.tbm.tudelft.nl/install.md)
- [Debugging and Monitoring](hosts/mgmt.bbb.tbm.tudelft.nl/debug.md)

### vcrNNN.bbb.tbm.tudelft.nl
- [Host Basics](hosts/vcrNNN.bbb.tbm.tudelft.nl/host.md)
- [Updating](hosts/vcrNNN.bbb.tbm.tudelft.nl/updating.md)
- [Installation](hosts/vcrNNN.bbb.tbm.tudelft.nl/install.md)
- [Debugging and Monitoring](hosts/vcrNNN.bbb.tbm.tudelft.nl/debug.md)

### turn.bbb.tbm.tudelft.nl
- [Host Basics](hosts/turn.bbb.tbm.tudelft.nl/host.md)
- [Updating](hosts/turn.bbb.tbm.tudelft.nl/upgrading.md)
- [Installation](hosts/turn.bbb.tbm.tudelft.nl/install.md)
- [Debugging and Monitoring](hosts/turn.bbb.tbm.tudelft.nl/debug.md)

### lb.bbb.tbm.tudelft.nl
- [Host Basics](hosts/lb.bbb.tbm.tudelft.nl/host.md)
- [Updating](hosts/lb.bbb.tbm.tudelft.nl/upgrading.md)
- [Installation](hosts/lb.bbb.tbm.tudelft.nl/install.md)
- [Debugging and Monitoring](hosts/lb.bbb.tbm.tudelft.nl/debug.md)

### frontend.bbb.tbm.tudelft.nl
- [Host Basics](hosts/frontend.bbb.tbm.tudelft.nl/host.md)
- [Updating](hosts/frontend.bbb.tbm.tudelft.nl/updating.md)
- [Installation](hosts/frontend.bbb.tbm.tudelft.nl/install.md)
- [Debugging and Monitoring](hosts/frontend.bbb.tbm.tudelft.nl/debug.md)

### Storage Box
- [Host Basics](hosts/u244865.your-storagebox.de/host.md)
- [Debugging and Monitoring](hosts/u244865.your-storagebox.de/debug.md)
