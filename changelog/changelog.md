# Infrastructure Changelog Description

## How to use
To use the changelog, add new entries for actions you took on the infrastructure to this file.
New entries go to the top of the 'Changes' section and have the following form:

```
## date (UTC, as supplied by `date` on the 18.04 hosts)
- *Change by:* <Operator Name>
- *Description:* Short description of the change. Try to make it fit into a tweet. If you need significantly more space, write a page and link it. [Example_ref](reports/unixtimestamp.md)
```

# Changes
## Thu Jun  1 07:46:04 CEST 2023
- *Change by:* Martijn Warnier
- *Description:* BBB servers are shutdown and cancelled


## Fri 19 Aug 2022 11:17:47 AM UTC
- *Change by:* Martijn Warnier
- *Description:* Installed new SSL certificates for bbb servers


## Thu Jun 23 15:31:20 UTC 2022
- *Change by:* Martijn Warnier
- *Description:* Upgraded bbb on vcr00X to version 2.4.9

## Fri Jun 10 16:03:41 UTC 2022
- *Change by:* Martijn Warnier
- *Description:* Upgraded bbb on vcr00X to version 2.4.8

## Sun 08 May 2022 09:07:01 AM UTC
- *Change by:* Martijn Warnier
- *Description:* Upgraded bbb on vcr00X to version 2.4.7


## Wed Apr 20 10:54:00 UTC 2022
- *Change by:* Martijn Warnier
- *Description:* Upgraded bbb on vcr00X to version 2.4.6


## Sat 12 Mar 2022 09:28:05 AM UTC
- *Change by:* Tobias Fiebig
- *Description:* After upgrade to 2.4.5 of bbb on vcr001 audio was broken. `bbb-conf --check` indicated necessary changes in `/etc/bigbluebutton/nginx/sip.nginx` (switch to https and port 7443); Applied changes, restarted nginx. Functionality was restored.

## Sat 12 Mar 2022 09:28:05 AM UTC
- *Change by:* Martijn Warnier
- *Description:* Permanently deleted users with greenlight account that did not contact admins after their accounts were deactivated in early February 

## Sun 13 Feb 2022 10:12:11 AM UTC
- *Change by:* Martijn Warnier
- *Description:* Updated bigbluebutton on vcr00X 

## Sat Feb 12 13:09:15 UTC 2022
- *Change by:* Tobias Fiebig
- *Description:* Adjust settings.yml preload file on vcr000 after upgrade to handle newly introduced variables.

## Thu 03 Feb 2022 06:03:37 PM UTC
- *Change by:* Martijn Warnier
- *Description:* Applied critical update to frontend

## Wed 02 Feb 2022 09:15:37 PM UTC
- *Change by:* Martijn Warnier
- *Description:*
  + Upgraded greenlight on production to version 2.11.1

## Wed 02 Feb 2022 04:07:10 PM UTC
- *Change by:* Martijn Warnier
- *Description:* 
  + Fixed two bugs in greenlight that occurred for the admin view (recordings and room settings)

## Tue Feb  1 18:45:46 UTC 2022
- *Change by:* Martijn Warnier
- *Description:* 
  + Ugraded all operating systems on system to latest version

## Tue 01 Feb 2022 04:50:22 PM UTC
- *Change by:* Martijn Warnier
- *Description:* 
  + Disabled non TU Delft users, except for a small list of exceptions
  + Updated banner to inform users that if they feel their account has wrongly been disabled to contact BBB admins

## Tue 01 Feb 2022 01:48:12 PM UTC
- *Change by:* Martijn Warnier
- *Description:* 
  + Updated greenlight for dev.bbb.tbm.tudelft.nl to version 2.11.1
  + Tested everything except for streaming, will continue later today 

## Thu Jan 27 10:43:03 UTC 2022
- *Change by:* Tobias Fiebig
- *Description:* 
  + Added nagios check for /boot

## Thu 27 Jan 2022 07:01:52 AM UTC
- *Change by:* Martijn Warnier
- *Description:* 
  + Updated system on frontend
  + Updated system on mgmt

## Sun Jan 23 19:21:18 UTC 2022
- *Change by:* Tobias Fiebig
- *Description:* 
  + Fix `apply-conf.sh` on vcr000 (removed `49 + ` and `50 + ` on lines 49 and 50 respectively
  + Fix `/etc/bigbluebutton/bbb-conf/permanent_configuration/archive.rb` on vcr000; File seems to have been a full HTML view of TUD gitlab; Reverted to correct file from repository in plain-text
  + Verified recording functionality for vcr000 and added it back to LB rotations
  + Reset dev.bbb. to the lb, instead of vcr000 individually

## Sun Jan 23 12:44:07 UTC 2022
- *Change by:* Martijn Warnier
- *Description:* 
  + Updated BBB on vcr000 to 2.4
  + Tested everything except for streaming
  + Unclear if recordings are processed correctly, will investigate later today. Other features all seem to work fine.

## Thu Jan 20 01:20:23 UTC 2022
- *Change by:* Tobias Fiebig
- *Description:* 
  + Fix update BBB on vcr001 to 2.4 to use correct TLS certificates, see [BBB 2.4 upgrade](../hosts/vcrNNN.bbb.tbm.tudelft.nl/upgrade24.md)
  + Enabled HSTS for vcr001 (part of upgrade documentation)
  + Enabled HSTS for lb and frontend (add `add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;` to nginx configuration)

## Wed Jan 19 22:00:41 UTC 2022
- *Change by:* Tobias Fiebig
- *Description:* 
  + Update BBB on vcr001 to 2.4, see [BBB 2.4 upgrade](../hosts/vcrNNN.bbb.tbm.tudelft.nl/upgrade24.md)
  + Switched the streaming container to a hand-build version (from https://github.com/aau-zid/BigBlueButton-liveStreaming), as the official docker images do not yet have 2.4 support.

## Wed Jan 19 18:05:55 UTC 2022
- *Change by:* Tobias Fiebig
- *Description:* Update 18.04: libruby2.5 nodejs ruby2.5 ruby2.5-dev ruby2.5-doc

## Wed Jan 19 14:28:12 UTC 2022
- *Change by:* Martijn Warnier
- *Description:* Changed on frontend.bbb /srv/greenlight/legal.md /srv/greenlight/terms.md - Added Martijn is new BBB admin  

## Mon Jan 17 14:00:24 UTC 2022
- *Change by:* Tobias Fiebig
- *Description:* Backup scripts on LB failed; Investigation revealed that cron on vcr000/001 were not running due to cron being unable to access the root account. Further investigation revealed that the root user was removed from /etc/shadow during the change adding Martijn Warnier's user account. The root account has been added back and normal operation has been verified.

## Wed Jan 12 18:25:37 UTC 2022
- *Change by:* Tobias Fiebig
- *Description:* Added administrative user for Martijn Warnier

## Wed Jan 12 11:29:35 UTC 2022
- *Change by:* Tobias Fiebig
- *Description:* Additional firewall rules to prevent bogons from reaching the Internet after Hetzner issued AbuseID:9D18EC:1B, see [report](reports/1641987055.md).

## Thu Jan  6 22:42:45 UTC 2022
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: apache2 apache2-bin apache2-data apache2-utils command-not-found libnss-systemd libpam-systemd libsystemd0 libudev1 linux-firmware linux-image-virtual openssh-client openssh-server openssh-sftp-server python3-commandnotfound qemu-guest-agent systemd systemd-sysv systemd-timesyncd udev
  + 18.04: libpam-systemd libsystemd-dev libsystemd0 libudev1 mongodb-org mongodb-org-mongos mongodb-org-server mongodb-org-shell mongodb-org-tools systemd systemd-sysv udev

## Fri Dec 24 15:20:44 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: docker-ce docker-ce-cli libpython3.8 libpython3.8-minimal libpython3.8-stdlib libseccomp2 linux-firmware python3.8 python3.8-minimal update-notifier-common
  + 18.04: bbb-config bbb-etherpad bbb-html5 docker-ce docker-ce-cli docker-ce-rootless-extras docker-scan-plugin nodejs
  + bbb: update to 2.3.17

## Sun Dec 12 00:27:30 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: busybox-initramfs busybox-static libnetplan0 libssl1.1 netplan.io openssl ubuntu-advantage-tools
  + 18.04: bbb-apps-akka bbb-config bbb-freeswitch-core bbb-fsesl-akka bbb-html5 bbb-record-core bbb-web bbb-webrtc-sfu libssl1.1 openssl
  + bbb: update to 2.3.16

## Thu Dec  2 22:41:13 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: libicu66 linux-image-virtual wget
  + 18.04: libsmbclient libwbclient0 python-samba python3-software-properties samba-common samba-common-bin samba-libs smbclient software-properties-common

## Wed Nov 24 16:59:09 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Fixed typo in greenlight for admin recording interface.

## Sat Nov 20 01:09:04 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: accountsservice cloud-init libaccountsservice0 libnetplan0 libssl1.1 netplan.io openssl rsync ubuntu-advantage-tools vim vim-common vim-runtime vim-tiny xxd
  + 18.04: containerd.io docker-ce docker-ce-cli docker-ce-rootless-extras rsync

## Thu Nov 11 21:36:13 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: libpq5 libtdb1 postgresql-12 postgresql-client-12 python3-software-properties software-properties-common
  + 18.04: libopenexr22 libpq5

## Wed Nov 10 19:28:51 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: libasound2 libasound2-data linux-base linux-firmware linux-image-virtual ubuntu-advantage-tools
  + 18.04: linux-base
  + turn.: Increase file-size limit and change to non-priv user.

## Sun Oct 31 11:38:26 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: tzdata
  + turn: bind9-dnsutils bind9-host bind9-libs
  + mgmt: libapache2-mod-php7.4 php7.4-cli  php7.4-common php7.4-json php7.4-opcache php7.4-readline

## Wed Oct 27 22:36:38 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + All: Upgrade docker, netplan 

## Tue Oct 19 21:23:28 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Upgrade to 2.10.0.2 and mitigate issue in recording visibility patch

## Tue Oct 19 16:42:07 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + revert to greenlight 2.9.2 (with avatar removal applied) after 2.10.0.1 broke recording visibility patch

## Sat Oct 16 23:52:37 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Deploy patches to greenlight that disable avatar support.

## Sat Oct 16 22:47:32 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Re-upgrade to greenlight 2.10.0.1 after applying workaround from https://github.com/bigbluebutton/greenlight/issues/2956

## Sat Oct 16 22:19:06 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Rollback greenlight to 2.9.2
  + https://github.com/bigbluebutton/bigbluebutton/issues/13501
  + https://github.com/bigbluebutton/greenlight/issues/2956

## Sat Oct 16 21:09:53 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Updated greenlight to 2.10.0.1

## Sat Oct 16 20:26:20 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 18.04: bbb-config bbb-etherpad bbb-freeswitch-core bbb-html5 bbb-libreoffice-docker bbb-playback bbb-web bbb-webrtc-sfu nodejs

## Sat Oct 16 20:15:41 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: cloud-init distro-info-data linux-firmware qemu-guest-agent

## Sat Oct  9 21:48:24 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: distro-info-data
  + 20.04-mgmt: apache2 apache2-bin apache2-data apache2-utils distro-info-data libntlm0

## Wed Oct  6 18:35:30 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: libpam-modules libpam-modules-bin libpam-runtime libpam0g libpython3.8 libpython3.8-minimal libpython3.8-stdlib python3.8 python3.8-minimal tzdata
  + 20.04-frontend/lb: containerd.io docker-ce docker-ce-cli
  + 18.04: containerd.io docker-ce docker-ce-cli docker-ce-rootless-extras libnetplan0 netplan.io nplan tzdata

## Sun Oct  3 18:50:05 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: libmysqlclient21, cloudinit, containerd.io
  + 18.04: containerd.io mongodb-org mongodb-org-mongos mongodb-org-server mongodb-org-shell mongodb-org-tools
  + Updated PP

## Tue Sep 28 16:16:41 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: libnetplan0 netplan.io python3-update-manager update-manager-core vim vim-common vim-runtime vim-tiny xxd
  + 20.04 (mgmt): apache2 apache2-bin apache2-data apache2-utils
  + 18.04: vim vim-common vim-runtime vim-tiny xxd
  + Moved backup 2h earlier during the night

## Mon Sep 27 12:29:39 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: ca-certificates libdrm-common libdrm2 linux-firmware linux-image

## Wed Sep 22 21:26:02 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 18.04: bbb-apps-akka bbb-config bbb-freeswitch-core bbb-html5 bbb-playback bbb-web bbb-webrtc-sfu curl libcurl3-gnutls libcurl4 libcurl4-openssl-dev linux-generic linux-headers-generic linux-image-generic linux-libc-dev snapd
  + Includes upgrade to BBB 2.3.14

## Wed Sep 22 21:20:07 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: libprocps8 libtiff5 linux-image-generic procps

## Sat Sep 18 14:42:29 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Update greenlight to 2.9.2

## Sat Sep 18 14:30:23 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: curl git git-man libcurl3-gnutls libcurl4 libgcrypt20 libnss-systemd libpam-systemd libsystemd0 libudev1 systemd systemd-sysv systemd-timesyncd udev wireless-regdb
  + 18.04: base-files libgnutls30 libmysqlclient20 libpam-systemd libsystemd-dev libsystemd0 libudev1 mongodb-org mongodb-org-mongos mongodb-org-server mongodb-org-shell mongodb-org-tools python-apt-common python3-apt python3-distupgrade systemd systemd-sysv ubuntu-release-upgrader-core udev update-notifier-common

## Thu Sep  9 22:33:44 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Exchanged all TUD provided TLS certificates

## Thu Sep  9 22:12:21 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: cpio libgd3 linux-image-generic python-apt-common python3-apt
  + 20.04-mgmt: cpio libapache2-mod-php7.4 libgd3 linux-image-virtual php7.4-cli php7.4-common php7.4-json php7.4-opcache php7.4-readline python-apt-common python3-apt
  + 18.04: cpio libgd3

## Tue Sep  7 15:24:52 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + BBB workers: Updated tutorial message to point to correct vhost (preventing cert error).

## Sat Sep  4 10:32:08 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: libntfs-3g883
  + 18.04: nodejs

## Mon Aug 30 14:40:48 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + greenlight update to 2.9.1

## Sat Aug 28 15:41:53 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: libssh postfix
  + 18.04: bbb 2.3.13

## Wed Aug 25 16:44:28 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: libssl1.1 openssl
  + 18.04: libssl1.1 openssl

## Thu Aug 19 19:03:22 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: linux-img
  + 18.04: libpam-systemd libsystemd-dev libsystemd0 libudev1 systemd systemd-sysv udev

## Sun Aug 15 22:18:12 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + 20.04: libpq5 linux-image-generic postgresql-12 postgresql-client-12 python3-distupgrade ubuntu-release-upgrader-core
  + 18.04: bbb-config bbb-html5 bbb-playback

## Thu Aug 12 09:34:58 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Update libssl1.1 openssh-client openssh-server openssh-sftp-server openssl on 20.04
  + Update libssl1.1 nodejs openssl on 18.04

## Fri Aug  6 21:26:44 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Update docker on 20.04
  + Update docker, kernel, BBB to 2.3.10

## Tue Aug  3 15:13:48 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Update 20.04 VMs: libdrm-common libdrm2 libgnutls30 login passwd shim shim-signed
  + Update 20.04 HW: libgnutls30 login passwd

## Sun Aug  1 21:00:17 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Update vcrs: bbb, containerd.io

## Sun 01 Aug 2021 08:37:19 PM UTC
- *Change by:* Tobias Fiebig
- *Description:* 
  + Update lb/frontend: containerd.io
  + Update privacy policy to clarify data processor role

## Thu Jul 29 08:25:04 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Update 20.04: libglib2.0-0 libglib2.0-bin libglib2.0-data libmysqlclient21 ubuntu-advantage-tools
  + Update 18.04 to 2.3.8: bbb-config bbb-html5 libsndfile1

## Thu Jul 22 23:38:56 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Updated systemd and curl

## Wed Jul 21 23:26:37 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Updated linux kernel and full reboot

## Tue Jul 20 22:38:55 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Updated greenlight to 2.9.0
  + Removed greenlight custom SAML patches, and migrated SSO connection to SURFconext's OpenID interface, see [the corresponding service documentation](../infrastructure_provider/provider.md).

## Tue Jul 20 18:43:55 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Updated BBB to 2.3.7
  + Updated docker on workers

## Tue Jul 20 18:11:22 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Updated non-workers; containerd update on docker hosts (lb/frontend)

## Tue Jul  6 21:46:46 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Updated BBB to 2.3.6

## Mon Jul  5 19:36:33 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Updated BBB to 2.3.5, docker, snap, kernel
  + lb/frontend: apt apt-transport-https apt-utils libapt-pkg6.0 libpython3.8 libpython3.8-minimal libpython3.8-stdlib linux-base linux-firmware python3.8 python3.8-minimal
  + other: +openssl/apache/several

## Mon Jul  5 12:19:13 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* 
  + Updated GL to 2.8.7; Patched out the new de-publishing feature from 2.8.5 in the rec_perm branch as this feature requires scalelite 1.1.

## Sun Jun 27 20:21:42 CEST 2021
- *Change by:* Arco van Geest
- *Description:* 
  + lb updated: gcc-10-base initramfs-tools initramfs-tools-bin initramfs-tools-core libatomic1 libgcc-s1 libgomp1 libstdc++6 nfs-common nfs-kernel-server linux-image-5.4.0-77-generic linux-modules-5.4.0-77-generic linux-modules-extra-5.4.0-77-generic
  + frontend updated: gcc-10-base initramfs-tools initramfs-tools-bin initramfs-tools-core libgcc-s1 libgomp1 libstdc++6 nfs-common linux-image-5.4.0-77-generic linux-modules-5.4.0-77-generic linux-modules-extra-5.4.0-77-generic

## Sun Jun 20 08:05:47 CEST 2021
- *Change by:* Arco van Geest
- *Description:* 
  + lb updated: libhogweed5 libnettle7 libnss-systemd libpam-systemd libprocps8 libsystemd0 libudev1 libxml2 procps systemd systemd-sysv systemd-timesyncd ubuntu-advantage-tools udev
  + frontend updated: libhogweed5 libnettle7 libnss-systemd libpam-systemd libprocps8 libsystemd0 libudev1 libxml2 procps systemd systemd-sysv systemd-timesyncd ubuntu-advantage-tools udev linux-image-generic

## Sat Jun 12 11:31:45 CEST 2021
- *Change by:* Arco van Geest
- *Description:* 
  + lb updated: intel-microcode linux-firmware update-notifier-common
  + frontend updated:  intel-microcode linux-firmware update-notifier-common


## Wed 09 Jun 2021 06:39:05 UTC
- *Change by:* Arco van Geest
- *Description:* Pinned scalelite version to v1.0. Recreated callback_data.

## Fri Jun  4 21:16:11 UTC 2021
- *Change by:* João Pizani and Arco van Geest
- *Description:*
  + Left over issue to be fixed: Nagios warning about stale recording older than 5min, caused by a table missing in the postgres DB (called `callback_data`). TBD ASAP.
  + updated lb:
    `containerd.io docker-ce docker-ce-cli grub-common grub-efi-amd64 grub-efi-amd64-bin grub-pc-bin grub2-common isc-dhcp-client isc-dhcp-common liblz4-1 libnginx-mod-http-image-filter libnginx-mod-http-xslt-filter libnginx-mod-mail libnginx-mod-stream libpolkit-agent-1-0 libpolkit-gobject-1-0 libpq5 libpython3.8 libpython3.8-minimal libpython3.8-stdlib libwebp6 linux-image-generic lz4 nginx nginx-common nginx-core policykit-1 postgresql-12 postgresql-client-12 python-apt-common python3-apt python3-distutils python3-lib2to3 python3.8 python3.8-minimal linux-image-5.4.0-74-generic linux-modules-5.4.0-74-generic linux-modules-extra-5.4.0-74-generic`
    
  + updated frontend:
    `linux-image-generic containerd.io docker-ce docker-ce-cli grub-common grub-efi-amd64 grub-efi-amd64-bin grub-pc-bin grub2-common isc-dhcp-client isc-dhcp-common liblz4-1 libnginx-mod-http-image-filter libnginx-mod-http-xslt-filter libnginx-mod-mail libnginx-mod-rtmp libnginx-mod-stream libpolkit-agent-1-0 libpolkit-gobject-1-0 libpq5 libpython3.8 libpython3.8-minimal libpython3.8-stdlib libwebp6 lz4 nginx nginx-common nginx-core policykit-1 postgresql-12 postgresql-client-12 python-apt-common python3-apt python3-distutils python3-lib2to3 python3.8 python3.8-minimal`


## Fri May 28 11:43:01 UTC 2021
- *Change by:* João Pizani
- *Description:*
  + Update `containerd.io libpam-modules libpam-modules-bin libpam-runtime libpam0g` on hosts `vcr000` and `vcr001` (18.04)

## Tue May 25 21:27:42 UTC 2021
- *Change by:* João Pizani
- *Description:*
  + Update `libpam0g libpam-{modules{,-bin},runtime} libx11-{6,data} python3-distupgrade ubuntu-release-upgrader-core` on hosts `frontend` and `lb` (20.04)

## Fri May 21 09:19:36 UTC 2021
- *Change by:* João Pizani
- *Description:*
  + Update `initramfs-tools initramfs-tools-bin initramfs-tools-core libssl1.1 openssl` on host `frontend` (20.04)
  + Update `initramfs-tools initramfs-tools-bin initramfs-tools-core` on hosts `vcr000` and `vcr001` (18.04)

## Thu May 20 13:30:55 UTC 2021
- *Change by:* João Pizani
- *Description:* Update `initramfs-tools initramfs-tools-bin initramfs-tools-core libssl1.1 openssl` on host `lb` (20.04)

## Thu May 20 11:23:26 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Reset password for user jpizaniflor, moved two bbb rooms from user tfiebig to new owners in the frontend.

## Tue May 18 12:47:27 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update dbus (20.04) and libdjvulibre (18.04)

## Mon May 17 09:09:22 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update dist-info (20.04) and intel-microcode (18.04)

## Thu May 13 18:51:57 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update netplan.io (20.04)

## Wed May 12 23:48:44 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Upgrade to BBB2.3, disable multiple recording workers.

## Tue May 11 17:10:35 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update python3-yaml, linux-kernel

## Mon May 10 17:53:39 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Disable logging after successful upg. to 2.3

## Sun May  9 00:24:26 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update grub (18.04)

## Sun May  9 00:21:26 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update to greenlight 2.8.4; Ignored CVE-2021-28965 by pinning REXML to 3.2.4. See: https://github.com/onelogin/ruby-saml/issues/577, https://github.com/omniauth/omniauth-saml/issues/199, https://github.com/department-of-veterans-affairs/va.gov-team/issues/23835

## Sat May  8 23:21:37 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update German translation, remove non-TLS TURN endpoint

## Thu May  6 20:58:56 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update kernel/firmware/updatemanager/asound (20.04) and mongo (18.04)

## Thu May  6 19:42:22 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update German translation according to https://github.com/bigbluebutton/bigbluebutton/issues/12294

## Fri Apr 30 22:09:05 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update BBB to 2.3.0

## Fri Apr 30 22:00:12 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update update-notifier, bind9 (20.04)

## Tue Apr 27 20:41:59 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update BBB to 2.3-rc2, update grub

## Sun Apr 25 21:54:16 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Change default camera order to 'local-presenter-alphabetical'

## Sat Apr 24 19:21:53 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Increase max FPS for screenshare to 30 to enable better video(+sound) sharing

## Sat Apr 24 16:02:05 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update BBB to 2.3-RC1; Update ubuntu-distupgrade (20.04 HW)

## Thu Apr 22 20:16:06 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update distro data, revert remaining vcr000 entries and flush redis cache vcr001

## Thu Apr 22 11:08:32 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Enable dynamic CORS for HLS directory

## Thu Apr 22 01:21:00 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update linuxfirmware (20.04)/ruby (18.04)

## Mon Apr 19 20:33:09 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* set `add_header Permissions-Policy interest-cohort=();` on all hosts, see https://paramdeo.com/blog/opting-your-website-out-of-googles-floc-network

## Sun Apr 18 20:09:02 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update vcr000 to beta-5; image vcr000 and reconfigure to vcr001. Update to GL2.8.3

## Sun Apr 18 11:25:53 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update vcr000 to beta-4; Taken out of rotation until fixes in beta-5 become available later today (soffice convert)

## Sun Apr 18 10:49:06 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update docker/systemd/kernel 20.04

## Sun Apr 11 00:03:31 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Redeploy 18.04 via image, upgrade to BBB2.3 and roll to prod (disabling vcr001). Upgrade scalelite to v1.0.12.1. Update streaming tool to support bbb2.3-beta-3

## Thu Apr  1 20:46:18 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Upgrade to 2.2.36

## Wed Mar 31 20:44:58 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Roll back vcr000 to 16.04/bbb2.2, image of 18.04 drawn; Updated curl

## Sun Mar 28 17:32:43 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update vcr000 to 18.04/bbb2.3, new installation documentation provided [here](../hosts/vcrNNN.bbb.tbm.tudelft.nl/install23.md).

## Sat Mar 27 01:44:23 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update recording playback to 2.3 player

## Fri Mar 26 00:04:46 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update linux kernel, isc dhcp, openssl

## Fri Mar 19 23:18:49 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update linux kernel and systemd, full reboot

## Tue Mar 16 00:28:47 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update to Greenligh 2.8, migrate to host-os psql in the process (install documentation adjusted accordingly), update linux kernel

## Wed Mar 10 22:28:15 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update sshd, container.d

## Fri Mar  5 00:09:58 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Updated workers to BBB 2.2.35 fixing bug in pad authentication. Enable storage of of notes, updated privacy policy for notes and streams, updated python-update-manager on 20.04

## Wed Mar  3 23:30:11 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Updated workers to BBB 2.2.34; Upgraded docker. [Change Report](reports/1614813922.md)

## Tue Mar  2 20:20:58 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* update libpq5 bind9 utils

## Mon Mar  1 12:46:47 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Deploy hotfix for streaming consent.

## Sat Feb 27 13:11:20 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Set `apt-mark hold docker-ce-rootless-extras docker-ce docker-ce-cli` on 16.04 (docker update breaking bbb-video-download); Pinned scalelite to 1.0.8 due to https://github.com/blindsidenetworks/scalelite/issues/448

## Sat Feb 27 11:16:58 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Migrate away from custom container after upstream merge; update docker and bbb to 2.2.33

## Fri Feb 26 00:56:27 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Build custom container for streaming until fix for https://github.com/aau-zid/BigBlueButton-liveStreaming/issues/109 is upstream

## Thu Feb 25 23:56:15 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update screen/kernel, update to bbb 2.2.32, install https://github.com/ichdasich/bbb-stream-control

## Tue Feb 23 18:46:20 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Update grub (20.04); reduce backup retention to 5d

## Thu Feb 18 22:35:55 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* update bind9/openssl/pciutils

## Tue Feb 16 20:44:34 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* update gpg/postgres et al.

## Mon Feb 15 14:42:59 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Deactivate logging (greenlight+nginx) for frontend/lb/vcr's; No event registered.

## Sun Feb 14 16:31:25 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Activate logging (greenlight+nginx) for frontend/lb/vcr's due to expected abuse case on Monday

## Fri Feb 12 20:44:09 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Updated TURN config

## Thu Feb 11 21:21:58 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Deploy greenlight 2.7.19

## Wed Feb 10 18:41:05 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Deploy greenlight 2.7.18 custom with new feature to restrict rooms to TUD users

## Wed Feb 10 00:01:12 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* update qemu additions, libldap, jre (16.04)

## Sun Feb  7 10:57:28 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* update ubuntu-drivers-common

## Tue Feb  2 23:54:14 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* update cacertificates/docker

## Sun Jan 31 11:51:31 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* update 20.04 python-update-manager

## Fri Jan 29 00:53:39 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* update linux kernel/tzdata/python-apt/linux-firmware

## Wed Jan 27 20:31:42 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* update sudo/linux image/libc

## Sat Jan 23 01:07:27 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Applied https://github.com/bigbluebutton/bigbluebutton/issues/11183 for testing (should be overwritten by next update)

## Fri Jan 22 11:13:55 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Updated 20.04 apt

## Wed Jan 20 21:31:18 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Updated 20.04 kernel image/netplan/systemd

## Fri Jan 15 00:39:36 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Updated kernel image

## Thu Jan 14 13:46:57 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Updated vcrs to use performance cpu scheduler

## Wed Jan 13 21:10:20 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Updated tar/tzdata/coturn/cloud-init

## Fri Jan  8 22:39:35 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Updated kernel/tzdata

## Fri Jan  8 01:20:18 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Updated ghostscript

## Wed Jan  6 01:10:03 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Updated linux kernel and docker

## Fri Jan  1 23:43:55 UTC 2021
- *Change by:* Tobias Fiebig
- *Description:* Updated to greenlight 2.7.14

## Sun Dec 20 01:49:05 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Updated to greenlight 2.7.12

## Sun Dec 20 01:11:56 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Update to BBB 2.2.31

## Wed Dec 16 01:47:29 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Update docker (20.04/16.04) and linux-kernel (20.04)

## Fri Dec 11 01:23:27 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Update wireless-regdb (20.04/16.04) and munin (16.04)

## Wed Dec  9 21:38:39 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Update docker.io, apt, activate static serving of selected meteor files

## Tue Dec  8 23:23:48 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Update libopenssl and rebooted infrastructure

## Sun Dec  6 00:43:30 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Update libuefi on 20.04

## Wed Dec  2 23:31:20 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Enable hiding of dial-in numbers for dial-in users, change privacy policy accordingly. See: https://docs.bigbluebutton.org/2.2/customize.html#add-a-phone-number-to-the-conference-bridge

## Wed Dec  2 23:06:09 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Update to greenlight 2.7.10 due to CVE-2020-29043

## Wed Dec  2 03:10:49 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Update docker-ce on docker hosts.

## Tue Dec  1 21:17:08 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Revert enabling custom hearbeat due to errors with timers.js in meteor main.js; Reported to BBB-ML; Github issue pending reaction there.

## Tue Dec  1 10:43:24 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Updated linux image (20.04), container.io (16.04)

## Tue Dec  1 10:40:56 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* There was some error on vcr000 which made the webclient inaccessible. Monitoring did not catch this. I added a monitoring check for it now. Will keep an eye on it, if this continues to occur, I will add an active monitoring check.

## Sun Nov 29 21:49:29 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Update to bbb 2.2.30; Important changes: https://github.com/bigbluebutton/bigbluebutton/pull/10826 and https://github.com/bigbluebutton/bigbluebutton/pull/10907 (set to true), enable niceAgentIceTcp=1 in WebRtcEndpoint.conf.ini, update freeswitch stun server; Note that the update change /etc/bigbluebutton/nginx/sip.nginx (http to https for proxy; Leads to sip failing for web clients)

## Fri Nov 27 01:33:49 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Update libpoppler on 16.04 (again), ubuntu-sys-up on 20.04

## Thu Nov 26 00:59:00 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Update libpoppler on 16.04

## Wed Nov 25 09:44:30 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Update busybox for initramfs on 20.04

## Mon Nov 23 18:07:51 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Update grub on all hosts.

## Sat Nov 21 23:04:49 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Reactivate three thread Kurento after applying fix in https://github.com/bigbluebutton/bigbluebutton/issues/10881; also disabled client logging

## Sat Nov 21 13:09:44 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Temporarily activate logging to debug screenshare issues with firefox, disable three-process kurento as probable root-cause

## Thu Nov 19 15:45:40 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Added nagios check for missing PSQL connection in recording-importer container

## Wed Nov 18 14:10:32 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Updated 20.04 hosts; linux kernel, psql, ldap, certbot, krb5

## Sat Nov 14 23:17:31 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Updated workers to BBB 2.2.29; [Change Report](reports/1605395851.md)

## Fri Nov 13 00:53:44 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Update libefi etc (20.04) and openjdk-8, and restart bbb (16.04)

## Thu Nov 12 09:10:53 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Update Intel microcode on physical machines.

## Wed Nov 11 08:59:59 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Update linux kernel image and dependencies.

## Tue Nov 10 22:03:50 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Updated `/etc/munin/plugins/bbb-users` to show TUD/non-TUD users and Privacy Policy Acceptance status (Active Accounts)

## Mon Nov  9 13:26:27 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Update libldap (16.04 and 20.04) and linux-firmware (20.04)

## Sat Nov  7 16:53:42 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Increased check frequency of all TLS checks to every 30 minutes

## Sat Nov  7 00:22:35 UTC 2020
- *Change by:* Tobias Fiebig
- *Description:* Infrastructure delivered to documentation; [Change Report](reports/example_report.md)
