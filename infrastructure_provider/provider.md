# Basics

We are currently hosted at Hetzner. Credentials for the hoster management
interface can be found in `/root/password` on mgmt.bbb.tbm.tudelft.nl, as 
this host also holds live credentials to the API (with similar permissions).

The login is at https://hetzner.de/; To manage the dedicated servers, please
login to the robot, for the two virtual machines into the cloud interface.

For the dedicated servers a remote KVM can be ordered for free in case there is
an issue with booting a machine.

# SURFconext

Authentication is being provided via SURFconext usen the OpenID interface. You
can configure the connection via https://sp.surfconext.nl/. To be able to
configure it, TU Delft central IT has to add you to the `BBB  TBM van de TU Delft`
service.

## Configuring OpenID in Greenlight
After configuring an OpenID connector on sp., you can add it to greenlight with
the following settings in `.env`:

```
# OpenID Connect Provider (optional)

# Provided when configuring the service
OPENID_CONNECT_CLIENT_ID=bbb.tbm.tudelft.nl

# Can be reset via the web-interface. A reset is instantanious
OPENID_CONNECT_CLIENT_SECRET=<secret>
# use https://connect.test.surfconext.nl for testing
OPENID_CONNECT_ISSUER=https://connect.surfconext.nl
# We are using the edu_principal_name field as it is the most static one
OPENID_CONNECT_UID_FIELD=eduperson_principal_name

# OAUTH2_REDIRECT allows you to specify the redirect_url passed to oauth on sign in.
# Necessary to be explicitly set with greenlight
OAUTH2_REDIRECT=https://bbb.tbm.tudelft.nl/b/
```
