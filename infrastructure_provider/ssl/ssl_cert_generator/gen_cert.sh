#!/bin/bash
hosts='
bbb.tbm.tudelft.nl
dev.bbb.tbm.tudelft.nl
lb.bbb.tbm.tudelft.nl
mgmt.bbb.tbm.tudelft.nl
turn.bbb.tbm.tudelft.nl
vcr000.bbb.tbm.tudelft.nl
vcr001.bbb.tbm.tudelft.nl
'

for h in $hosts;
do
	openssl req -new -config $h.txt -newkey rsa:4096 -nodes -keyout $h.key -out $h.csr
done;
