[req]
default_bits = 4096
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn

[ dn ]
C=NL
ST=Zuid-Holland
L=Delft
O=TU Delft
OU=TBM-ESS-ICT
emailAddress=bbb-ess-ict-tbm@tudelft.nl
CN = mgmt.bbb.tbm.tudelft.nl

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = mgmt.bbb.tbm.tudelft.nl
