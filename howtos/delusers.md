# Non-TUD User Deletion Script
To delete non-TU Delft users, a small script has been created and stored in tools/delete_users/
This script should be executed in two steps:
1. Accounts are disabled and marked for deletion
2. After a cool-down period (e.g., two weeks) all accounts can be deleted
The cool-down period serves as a safe-guard in case people forgot to request an extension of their accounts.

The script has to be executed as the root user on frontend.bbb.tbm.tudelft.nl (sudo is sufficient).
Two files are relevant: `delete_users.py` und `exceptions`.

## 1. Filling exceptions
To fill the exceptions file, add one email address of non-TU Delft user accounts that should be retained per line.

## 2. Preparing deletion
To prepare for deleting users, the script first has to be configured. Specifically:

1. Change line 10 to `envi="PROD"` to execute on the production environment
2. Add the PSQL password to line 12. This can be found in `/srv/greenlight/.env`

## 3. Executing deletion
Execute the script as root.

## 4. Preparing permanent removal of accounts
After a cool-down period, proceed to permanently delete accounts. Keep in mind that the `exceptions` file _MUST_ remain in place, as removal will take place independen of whether the account has previously been deleted.

1. Change line 10 to `envi="PROD"` to execute on the production environment
2. Change line 11 to `dst="REM"` to permanently remove and not just disable accounts
3. Add the PSQL password to line 12. This can be found in `/srv/greenlight/.env`

## 5. Executing removal
Execute the script as root.
