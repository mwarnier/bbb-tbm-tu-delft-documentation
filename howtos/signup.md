# Signup flow
This documents the changes made to TU Delft's version of greenlight to allow openID users to be directly authenticated without administrative intervention, while restricting access for non-TU Delft users.

## Changes to greenlight (codebase)
To enable this change, i.e., to exclude TU Delft users from the approval mechanic, the following change was applied to greenlight in the `openid-changes` branch:
```
diff --git a/app/controllers/sessions_controller.rb b/app/controllers/sessions_controller.rb
index 0ae2f8da..fe182e13 100644
--- a/app/controllers/sessions_controller.rb
+++ b/app/controllers/sessions_controller.rb
@@ -229,12 +229,13 @@ class SessionsController < ApplicationController
     logger.info "Support: Auth user #{user.email} is attempting to login."
 
     # Add pending role if approval method and is a new user
-    if approval_registration && !@user_exists
+    if approval_registration && !@user_exists && @auth['provider'] != "openid_connect"
       user.set_role :pending
 
       # Inform admins that a user signed up if emails are turned on
       send_approval_user_signup_email(user)
 
+      logger.info "Support: Setting auth user #{user.email} to pending (#{auth['provider']})."
       return redirect_to root_path, flash: { success: I18n.t("registration.approval.signup") }
     end
```
This excludes all users authenticating via `openid_connect` (SURFconext) from the pending state.

## Deploy
At the moment, the change in `openid-changes` is merged into `custom-v21002` but has only been deployed to dev.; To execute the change, it must first be deployed to production.
Thereafter, to enable restricted access, it has to be enabled in the administrative interface. The path to the setting is:
```
Organization -> Site Settings -> Settings -> Registration Method
```
There, in the pull-down menu, `Approve/Decline` has to be selected. No additional saving of the setting is required. In addition, the same configuration can be done in the docker container's .env file, but the configuration in the database takes precedence.
