# Deleting administrative accounts

Deleting an administrative account is an important task upon the departure of a system administrator.
Furthermore, secret keys and credentials should be exchanged.
This document lists the spots and places where changes and re-keying have to take place.

## User account removal

### System administration accounts
The first spot where an account must be replaced is the linux system itself.
For this, execute the following command on all hosts:
```
userdel -rf $user
```

### Monitoring accounts
The administrative users' access to icinga has to be removed. For that, remove the account from the following files on mgmt.bbb:
```
/etc/nagios4/htdigest.users
/etc/nagios4/objects/contacts.cfg
```
Keep in mind, that administrative users have to removed from the contact group as well (contacts.cfg).

Thereafter, restart nagios and apache:
```
service apache2 restart
service nagios4 restart
```

### BBB Users
An administrative user most likely has two accounts in the BBB web frontend (one TU Delft account and one non-TU Delft account to retain access in case of SSO failures).
In the webinterface, go to:
```
Organization -> Manage Users
```
Use the search field to search for 'admin' and search for the user. Then, delete the non-SSO account and remove administrative permissions from the SSO accounts by selecting the three dots on the right, and thereafter 'Edit'.

### Hetzner Account Password/Email change
For the webinterface at Hetzner, the password and email addresses have to be changed.
For this purpose, go to hetzner.de and log in to the Robot.
Under settings-> account settings (top right corner) remove the departing administrator's email address from all fields, and set a new password.

## Re-Keying/Change of credentials

There are several credentials used throughout the setup which _could_ enable a former administrative user to breach the system despite their main account having been removed.
As such, these items should be re-keyed.

### Backup Encryption Key

We are currently performing backups to a hetzner storage box located in Helsinki.
As this is a storage where we do not have direct hardware control, backups are encrypted before being send over.
Encryption takes place on line 33 of `/etc/cron.daily/backup` on lb.bbb.;
```
... | openssl enc -e -aes256 -pbkdf2 -k '<ENTER NEW KEY HERE>' -out $backupfile
```
It is strongly advised to create a physical copy of this key and store it in a safe place to guard against complete system failure.
Re-keying of old backups is not adviced, and after five days the retention policy will have lead to all backups having the new key.

### BBB Secrets
BBB is authenticated via secret strings. These have to be changed in multiple places:

#### BBB Loadbalancer
The secret key for the loadbalancer (`LOADBALANCER_SECRET`) is in `/etc/default/scalelite` on lb.bbb.
Change this to a long random string and then restart scalelite. Thereafter, the secret key on the two Greenlight instances has to be updated in `/srv/greenlight/.env` and `/srv/greenlight-surf-dev/.env`.
Thereafter the corresponding greenlight containers have to be restarted.

Furthermore, you must change the BBB secret in `/opt/bbb-stream-control/controller.py`.

#### BBB Nodes
To change the secrets of the BBB nodes, they first have to be removed in scalelite on lb using:
```
docker exec -it scalelite-api ./bin/rake servers:remove[<ID>]
```

Next, on the corresponding node, edit `/etc/bigbluebutton/bbb-web.properties` and change the `securitySalt` to a new value.
Thereafter, restart BBB using `bbb-conf --restart`.

Finally, readd the server on the loadbalancer using:
```
docker exec -it scalelite-api ./bin/rake servers:add[<URL>,<SECRET>,1]
```
URL here is the BBB URL (see `/etc/bigbluebutton/bbb-web.properties`), and the secret the newly set secret. See also: https://github.com/blindsidenetworks/scalelite

Note: Technically, there is also `servers:update[]`, however, I am not sure whether this is available in the Scalelite version we are running.

#### Adjusting monitoring
The monitoring currently uses hardcoded calls to the BBB API of LB/the VCRs to check their health.
These have to be updated in `/etc/nagios4/objects/services.cfg`, in lines 22, 54, and 65.

These can be generated with the following python snippet:
```
#!/usr/bin/python3
import hashlib
URL = '<API URL>'
# URL = 'https://vcr000.bbb.tbm.tudelft.nl/bigbluebutton/api/'
ACTION = 'getMeetings'
# ACTION = 'getRecordings?meetingID=3e9695c1b97fedec05874c833bd1e39da3925d90'
h = hashlib.sha1((ACTION).encode('utf-8'))
checksum = h.hexdigest()

if '?' in ACTION:
	print(URL+ACTION+'&checksum='+checksum)
else:
	print(URL+ACTION+'?checksum='+checksum)
```

### Ruby Application Secret Keys

Ruby-on-Rails uses `SECRET_KEY_BASE`s to sign and encrypt cookies. This has to be changed in three places:
1. On the loadbalancer in `/etc/default/scalelite`, restart scalelite thereafter using `systemctl restart scalelite.target`
2. In the .evn files of the greenlight instances (`/srv/greenlight/.env` and `/srv/greenlight-surf-dev/.env`) on frontend. Change these two and then restart the corresponding containers.

### Hetzner API Tokens

We are currently using a hetzner API token to monitor the storage box utilization in `/opt/monitoring/check_storage_box` and `/etc/munin/plugins/storage-box`.
The password there can be changed in the hetzner robot under settings (top right corner). 
No service restarts are necessary after changing these two files.

### Nagios SSH Key

Nagios monitors local statistics using SSH with key-based authentication.
To roll this key, replace the files in `/var/lib/nagios/.ssh`

Thereafter, on all machines, `/home/monitoring/.ssh/authorized_keys` has to be updated to the new keys.

### Backup SSH Key

Depositing the backups on the storage box is authenticated via an SSH key (`/root/.ssh` on lb.bbb.). 
To change that key, please refer to https://docs.hetzner.com/robot/storage-box/access/access-ssh-rsync-borg/.

### Storage Box Password

The storage box has a sub-user who can only access the backups folder for rsync.
The password can be changed in the robot webinterface of Hetzner.
This password is used for monitoring backup change in `/opt/monitoring/check_backup`, and has to be changed there after changing it in the webinterface.

### Nagios Web Passwords
Nagios web-passwords of other administrators should be updated after and administrator left.
See the nagios account removal instructions for further information.

### User Account Passwords
All administrative users should change their user passwords for sudo after an administrator departed.

### TU Delft Mail Relay
We are currently using a mail relay via TU Delft on mgmt. and frontend.
A change can be requested via topdesk.
Credentials are stored in `/etc/postfix/sasl_passwd`.
After changing this file, execute:
```
postmap /etc/postfix/sasl_passwd

```

### TURN Server Secret

Our turn-server is instrumented by the BBB nodes via a shared secret. This shared secret can be changed on the turn server in `/etc/turnserver.conf`, line 43.
After changing the secret, restart turn with:
```
service coturn restart
```

Subsequently, you have to change `/etc/bigbluebutton/turn-stun-servers.xml` on both BBB nodes to the new secret and restart bbb using:
```
bbb-conf --restart
```

### TLS Certificates
All TLS certificates should be regenerated, including re-keying.
This can be done with the toolchain in `../infrastructure_provider/ssl` in this repository.
Certificates must be requested via topdesk/TU Delft.

NOTE: TU Delft is very adamant about trying to have BBB use a wildcard certificate (`*.bbb.tbm.tudelft.nl`). 
This is a bad idea, because it means that key-material is shared between the frontend and the backends.
However, BBB backends did not have the best security track record so far; Hence, it is strongly suggested to _NOT_ share key material between workers/turn/lb/frontend!

### PSQL Frontend
To change the PSQL password on frontend, log in, and use the postgres user to connect to psql (trust based, only with sudo):
```
sudo -u postgres psql
```

You can then change the password as follows:
```
postgres=# \password greenlight_production
Enter new password: <new-password>
postgres=# \q
```

#### Update dependent scripts
Thereafter, the password for psql has to be changed in the following scripts on frontend:
```
/etc/munin/plugins/bbb-rooms
/opt/bbb-stream-control/controller.py
```
In addition, you have to change the secret for the monitoring calls in `/etc/nagios4/objects/services.cfg` on mgmt.bbb.

#### Update greenlight
To update greenlight to the new password, change `/srv/greenlight/.env` and thereafter restart the greenlight container.

### PSQL Loadbalancer

To change the PSQL password on the loadbalancer, log in, and use the postgres user to connect to psql (trust based, only with sudo):
```
sudo -u postgres psql
```

You can then change the password as follows:
```
postgres=# \password scalelite
Enter new password: <new-password>
postgres=# \q
```

#### Update password for scalelite
To change the password for scalelite's PSQL access, change it on the loadbalancer in `/etc/default/scalelite`, restart scalelite thereafter using `systemctl restart scalelite.target`

#### Update dependent scripts
After changing the psql password, update the password in `/opt/bbb-tools/create_dl_link.py`.

In addition, you have to change the secret for the monitoring calls in `/etc/nagios4/objects/services.cfg` on mgmt.bbb.
