# Howtos
This folder contains several howtos. Currently:

- [Deleting an administrative user](deladmin.md)
- [Activating permission based sign-up for non-TU Delft users](signup.md)
- [Deleting non-TU Delft users (w. allow list)](delusers.md)
