# General Debug

# Monitoring Debug
This section lists the monitoring scripts running for this host and details their debug pathways.

## Check Backup State

### Description
Checks that the newest backup is not older than 1 day (effectively 32h; Critical if it is older) and that the change in size is below 10% (warning for more than +-10%, critical for more than +-20%).

### Debug and Resolution
Check on lb., whether the backup script ran cleanly. Investigate also if there has been a large amount of recordings added/remove. In case of issues, restore from backups.

## Storage Box Disk Usage

### Description
Checks the utilization of the storage box vs. the Hetzner API.

### Debug and Resolution
This get too small as soon as `7*(combined_size_of_recordings)` is larger than 2TB. Upgrade if this happens.
