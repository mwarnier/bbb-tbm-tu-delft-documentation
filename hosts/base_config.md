# Base configuration:

Installed Software

- vlan
- vim
- htop
- screen
- zsh
- etckeeper
- tcpdump
- nload
- bc


# etckeeper
/etc/ on all systems is tracked in git with etckeeper. Use `etckeeper commit` before and after doing changes in /etc/.


# System base settings
All systems use /bin/zsh as login shell for root, etckeeper for /etc, UTC as TZ 
All operators have user accounts with sudo. There should be no logins as root.

Special dotfiles/configuration:

- .vimrc .vim .zshrc in /root
- prompt_gentoo_setup in /usr/share/zsh/Prompts/prompt_gentoo_setup
- Disable password auth for sshd

# Munin

Additional Software on all systems:

- munin-node 
- munin-plugins-core 
- munin-plugins-extra

# Ferm

See individual hosts for specific firewall configuration

For munin on all:
```
domain (ip) {
    table filter chain INPUT {
        saddr (95.217.131.128/32) proto (tcp) dport 4949 ACCEPT;
        proto tcp dport 4949 REJECT;
    }
}
```

# Nagios
```
useradd -m -G users monitoring
passwd monitoring # (Set random password)
```

Create ssh key on mgmt node for user nagios and deploy to monitoring users:

```
no-pty,no-agent-forwarding,no-port-forwarding
```

before key in ~/.ssh/authorized keys

sudo to nagios on mgmt and ssh into all hosts to fill known_hosts


## install monitoring plugins
```apt-get install monitoring-plugins monitoring-plugins-standard monitoring-plugins-common```

place /opt/monitoring/check_raid on hw raid hosts (frontend and lb)

```
#!/bin/bash
#
# Created by Sebastian Grewe, Jammicron Technology
#

# Get count of raid arrays
RAID_DEVICES=`grep ^md -c /proc/mdstat`

# Get count of degraded arrays
#RAID_STATUS=`grep "\[.*_.*\]" /proc/mdstat -c`
RAID_STATUS=`egrep "\[.*(=|>|\.).*\]" /proc/mdstat -c`

# Is an array currently recovering, get percentage of recovery
RAID_RECOVER=`grep recovery /proc/mdstat | awk '{print $4}'`
RAID_RESYNC=`grep resync /proc/mdstat | awk '{print $4}'`
RAID_CHECK=`grep check /proc/mdstat | awk '{print $4}'`

# Check raid status
# RAID recovers --> Warning
if [[ $RAID_RECOVER ]]; then
STATUS="WARNING - Checked $RAID_DEVICES arrays, recovering : $RAID_RECOVER"
EXIT=1
elif [[ $RAID_RESYNC ]]; then
STATUS="WARNING - Checked $RAID_DEVICES arrays, resync : $RAID_RESYNC"
EXIT=1
elif [[ $RAID_CHECK ]]; then
STATUS="OK - Checked $RAID_DEVICES arrays, check : $RAID_CHECK"
EXIT=0
# RAID ok
elif [[ $RAID_STATUS == "0" ]]; then
STATUS="OK - Checked $RAID_DEVICES arrays."
EXIT=0
# All else critical, better save than sorry
else
EXTEND_RAID_STATUS=`egrep "\[.*(=|>|\.|_).*\]" /proc/mdstat | awk '{print $2}' | uniq -c | xargs echo`
STATUS="WARNING- Checked $RAID_DEVICES arrays, $RAID_STATUS have failed check: $EXTEND_RAID_STATUS "
EXIT=1
fi

# Status and quit
echo $STATUS
exit $EXIT
```

## Bandwidth monitoring
place in /opt/monitoring/check_bandwidth

```
#!/bin/bash

# ============================== SUMMARY =====================================
#Author : Ken Roulamellah
#Date : 19/07/2018
#Version : 1.0
# Licence : GPL
# ===================== INFORMATION ABOUT THIS PLUGIN ========================
#
# This plugin checks the average RX and TX bandwidth utilisation. It use
# kbytes as measure unite.
#
# ========================== START OF PROGRAM CODE ===========================

STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3

function print_usage()
{
  echo "Usage :"
  echo "$0 [ -i=INTERFACE]  [ -ct=COUNT ] -w WARNING -c CRITICAL"
  echo "This script calculate the average bandwith usage."
  echo "Default values | interface: eth0, counter: 10"
}

interface=$( ip route | grep default | awk '{print $5}')
counter=10
warning=-1
critical=-1

sum_rx=0
sum_tx=0
avg_rx=
avg_tx=
i=


if [[ $# -lt 4 ]];
then
	echo "Error: Arguments are missing"
	print_usage
	exit $STATE_UNKNOWN
fi

while [[ $# -gt 0 ]]; do
    case "$1" in
        -i=*)
            interface="$(cut -d'=' -f2 <<<"$1")"
            shift
        ;;
        -ct=*)
            counter="$(cut -d'=' -f2 <<<"$1")"
            shift
        ;;
        -w)
            warning=$2
            shift 2
        ;;
        -c)
            critical=$2
            shift 2
        ;;
        *)
            printf "\nError: Invalid option '$1'"
            print_usage
            exit $STATE_UNKNOWN
        ;;
    esac
done

if [ $warning -lt 0 ] || [ $critical -lt 0 ];
then
	echo "Error: You need to specify a warning and critical treshold"
	print_usage
    exit $STATE_UNKNOWN
fi

grep -q "up" /sys/class/net/$interface/operstate || exec echo "$interface: no such device or down"

read rx <"/sys/class/net/$interface/statistics/rx_bytes"
read tx <"/sys/class/net/$interface/statistics/tx_bytes"

i=$counter
while [ $i -gt 0 ]; do
    sleep 1
    read newrx <"/sys/class/net/$interface/statistics/rx_bytes"
    read newtx <"/sys/class/net/$interface/statistics/tx_bytes"

    #echo "old rx :$rx"
    #echo "new rx :$newrx"
    rx_cal=$(bc <<< "scale=2; ($newrx-$rx) / 1000")
    tx_cal=$(bc <<< "scale=2; ($newtx-$tx) / 1000")

    sum_rx=$(bc <<< "scale=2;$sum_rx+$rx_cal")
    sum_tx=$(bc <<< "scale=2;$sum_tx+$tx_cal")

    #echo  "$interface {rx: $rx_cal ko/s, tx: $tx_cal ko/s}"
    rx=$newrx
    tx=$newtx
    ((i --))
done

avg_rx=$(bc <<< "scale=2;$sum_rx/$counter")
avg_tx=$(bc <<< "scale=2;$sum_tx/$counter")

#echo "$avg_rx"
#echo "$avg_tx"


if [ $(bc <<< "$avg_rx > $critical || $avg_tx > $critical") -eq 1 ]; then
	echo "$interface CRITICAL - AVG_RX: $avg_rx kb/s,  AVG_TX: $avg_tx kb/s | 'Average Bandwidth RX'="$avg_rx";0;0;0;10000 'Average Bandwidth TX'="$avg_tx";0;0;0;10000"
	exit $STATE_CRITICAL
elif [ $(bc <<< "$avg_rx > $warning || $avg_tx > $warning") -eq 1 ]; then
	echo "$interface WARNING - AVG_RX: $avg_rx kb/s,  AVG_TX: $avg_tx kb/s | 'Average Bandwidth RX'="$avg_rx";0;0;0;10000 'Average Bandwidth TX'="$avg_tx";0;0;0;10000"
	exit $STATE_WARNING
else
	echo "$interface - OK AVG_RX: $avg_rx kb/s,  AVG_TX: $avg_tx kb/s | 'Average Bandwidth RX'="$avg_rx";0;0;0;10000 'Average Bandwidth TX'="$avg_tx";0;0;0;10000"
	exit $STATE_OK
fi
exit 3
```

## Install unattended upgrade

```
apt-get install unattended-upgrades
```
