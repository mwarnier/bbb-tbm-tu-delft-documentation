# turn.bbb.tbm.tudelft.nl
```
IPv4: 78.47.159.122
IPv6: 2a01:4f8:c17:b0ea::1
IPv4 Backend (vid 4000): none
Location: Virtual, Falkenstein, DE
Purpose: TURN services for bbb
Services: coturn
OS: Ubuntu 20.04
Comment: Virtual Machine
```
