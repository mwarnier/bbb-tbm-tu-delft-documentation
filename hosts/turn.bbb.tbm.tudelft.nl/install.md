# Additional Software
Install: 
- certbot
- coturn

# certbot

Initialize Certificate

```
# certbot certonly --standalone 
Certificate created for turn.bbb.tbm.tudelft.nl; check presence of /etc/cron.d/certbot
```

Note: TU Delft required certificates issued via SURF. These are in /etc/ssl/private/


# coturn
```
# /etc/turnserver.conf
# Example coturn configuration for BigBlueButton

# These are the two network ports used by the TURN server which the client
# may connect to. We enable the standard unencrypted port 3478 for STUN,
# as well as port 443 for TURN over TLS, which can bypass firewalls.
listening-port=3478
tls-listening-port=443

# If the server has multiple IP addresses, you may wish to limit which
# addresses coturn is using. Do that by setting this option (it can be
# specified multiple times). The default is to listen on all addresses.
# You do not normally need to set this option.
#listening-ip=172.17.19.101

# If the server is behind NAT, you need to specify the external IP address.
# If there is only one external address, specify it like this:
external-ip=78.47.159.122

# If you have multiple external addresses, you have to specify which
# internal address each corresponds to, like this. The first address is the
# external ip, and the second address is the corresponding internal IP.
#external-ip=172.17.19.131/10.0.0.11
#external-ip=172.17.18.132/10.0.0.12

# Fingerprints in TURN messages are required for WebRTC
fingerprint

# The long-term credential mechanism is required for WebRTC
lt-cred-mech

# Configure coturn to use the "TURN REST API" method for validating time-
# limited credentials. BigBlueButton will generate credentials in this
# format. Note that the static-auth-secret value specified here must match
# the configuration in BigBlueButton's turn-stun-servers.xml
# You can generate a new random value by running the command:
#   openssl rand -hex 16
use-auth-secret
static-auth-secret=<removed for documentation>

# If the realm value is unspecified, it defaults to the TURN server hostname.
# You probably want to configure it to a domain name that you control to
# improve log output. There is no functional impact.
# realm=example.com
realm=bbb.tbm.tudelft.nl

# Configure TLS support.
# Adjust these paths to match the locations of your certificate files
cert=/etc/letsencrypt/live/turn.bbb.tbm.tudelft.nl/fullchain.pem
pkey=/etc/letsencrypt/live/turn.bbb.tbm.tudelft.nl/privkey.pem

# Limit the allowed ciphers to improve security
# Based on https://hynek.me/articles/hardening-your-web-servers-ssl-ciphers/
cipher-list="ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384"

# Enable longer DH TLS key to improve security
dh2066

keep-address-family

# All WebRTC-compatible web browsers support TLS 1.2 or later, so disable
# older protocols
no-tlsv1
no-tlsv1_1

# Log to a single filename (rather than new log files each startup). You'll
# want to install a logrotate configuration (see below)
log-file=/var/log/coturn.log
simple-log

# Drop privileges
unpriv_user = turnserver
```

Adjust system file to run as root and drop priv to align with listening on port 443:
```
# sed -i s/User=turnserver/User=root/ /lib/systemd/system/coturn.service
# systemctl daemon-reload
```

Restart coturn and create logfile:
```
# touch /var/log/coturn.log
# chown turnserver: /var/log/coturn.log
# service coturn restart
```
# Disable Logging
```
ln -sf /dev/null /var/log/coturn.log 
```
Adjust if necessary for debugging.
