# Installed Software
Install:
- ferm
- nfs-kernel-server

# ferm
```
# -*- shell-script -*-
#
# /etc/ferm/ferm.conf

# Default rules
domain (ip ip6) {
    table filter {
        # Default Policies
        chain INPUT policy DROP;
        chain OUTPUT policy ACCEPT;

        # loopback traffic
        chain INPUT interface lo ACCEPT;
        chain OUTPUT outerface lo ACCEPT;

        chain (INPUT OUTPUT) {
            # ICMP is very handy and necessary
            proto icmp ACCEPT;

            # connection tracking
            mod conntrack ctstate (RELATED ESTABLISHED) ACCEPT;
        }
    }
}

domain (ip ip6) {
    table filter {
        chain (DOCKER DOCKER-USER DOCKER-INGRESS DOCKER-ISOLATION-STAGE-1 DOCKER-ISOLATION-STAGE-2 FORWARD) @preserve;
    }

    table nat {
        chain (DOCKER DOCKER-INGRESS PREROUTING OUTPUT POSTROUTING) @preserve;
    }
}


domain (ip ip6) {
    table filter chain INPUT proto tcp dport 22 ACCEPT;
    table filter chain INPUT interface enp35s0.4000 ACCEPT;
    table filter chain INPUT proto tcp dport (6379 5432) ACCEPT;
}
domain (ip) {
    table filter chain INPUT proto tcp dport 80 ACCEPT;
    table filter chain INPUT proto tcp dport 443 ACCEPT;
    table filter chain INPUT proto tcp dport 880 ACCEPT;
    table filter chain INPUT proto tcp dport 8443 ACCEPT;
}
domain (ip6) {
    table filter chain INPUT proto tcp dport 80 ACCEPT;
    table filter chain INPUT proto tcp dport 443 ACCEPT;
    table filter chain INPUT proto tcp dport 880 ACCEPT;
    table filter chain INPUT proto tcp dport 8443 ACCEPT;
}
```

# NFS

Create folders:
```
# mkdir -p /srv/nfsroot/recordings/var/bigbluebutton/spool/
# mkdir -p /srv/nfsroot/recordings/var/bigbluebutton/published
# mkdir -p /srv/nfsroot/recordings/var/bigbluebutton/recording
# mkdir -p /srv/nfsroot/recordings/var/bigbluebutton/unpublished
# mkdir -p /srv/nfsroot/backups/frontend.bbb.tbm.tudelft.nl
# mkdir -p /srv/nfsroot/backups/vcr000.bbb.tbm.tudelft.nl
# mkdir -p /srv/nfsroot/backups/vcr001.bbb.tbm.tudelft.nl
```

Set permissions for bbb:
```
# chown -Rv 1000:1000 /srv/nfsroot/recordings/var/bigbluebutton/published /srv/nfsroot/recordings/var/bigbluebutton/unpublished
# chown -Rv 1000:2000 /srv/nfsroot/recordings/var/bigbluebutton/spool/
```

Create /etc/exports:
```
/srv/nfsroot/recordings/var/bigbluebutton/spool/                                 10.23.42.0/24(rw,no_root_squash,async,insecure, no_subtree_check)
/srv/nfsroot/backups/frontend.bbb.tbm.tudelft.nl                                 10.23.42.2/32(rw,no_root_squash,async,insecure,no_subtree_check)
/srv/nfsroot/backups/vcr000.bbb.tbm.tudelft.nl                                 10.23.42.100/32(rw,no_root_squash,async,insecure,no_subtree_check)
/srv/nfsroot/backups/vcr001.bbb.tbm.tudelft.nl                                 10.23.42.101/32(rw,no_root_squash,async,insecure,no_subtree_check)
```

Re-export:
```
# exportfs -a
```

# Scalelite
## Redis
```
# apt-get install redis
```

Change /etc/redis/redis.conf to ‘listen 10.23.42.1’


## Psql
```
# apt-get install postgresql postgresql-contrib
lb.bbb.tbm.tudelft.nl ~ # sudo -u postgres createuser --interactive
Enter name of role to add: scalelite
Shall the new role be a superuser? (y/n) y
lb.bbb.tbm.tudelft.nl ~ # sudo -u postgres createdb scalelite
lb.bbb.tbm.tudelft.nl ~ # sudo -u postgres psql
psql (12.4 (Ubuntu 12.4-0ubuntu0.20.04.1))
Type "help" for help.

postgres=# \password postgres
Enter new password:
Enter it again:
postgres=# \password scalelite
Enter new password:
Enter it again:
postgres=# \q
```

Change in /etc/postgresql/12/main/hba.conf
```
# IPv4 backend connections:                                                     
host    all       scalelite       172.16.0.0/12           md5                   
host    all       scalelite       10.0.0.0/8           md5                      
host    all       scalelite       192.168.0.0/16           md5   
```

Change in /etc/postgresql/12/main/postgresql.conf 
```
listen_addresses = '10.23.42.1'
```

## Scalelite config
In /etc/default/scalelite
```
URL_HOST=lb.bbb.tbm.tudelft.nl
SECRET_KEY_BASE=<redacted_for_documentation>
LOADBALANCER_SECRET=<redacted_for_documentation>DATABASE_URL=postgresql://scalelite: <redacted_for_documentation>@10.23.42.1/scalelite
REDIS_URL=redis://10.23.42.1
SCALELITE_TAG=v1
SCALELITE_RECORDING_DIR=/srv/nfsroot/recordings/var/bigbluebutton/
NGINX_SSL=true
SCALELITE_NGINX_EXTRA_OPTS=--mount type=bind,source=/etc/letsencrypt,target=/etc/nginx/ssl,readonly
POLL_INTERVAL=60
RECORDING_IMPORT_POLL=true
RECORDING_IMPORT_POLL_INTERVAL=60
WEB_CONCURRENCY=24
RAILS_ENV=production
```

## Scalelite docker
We follow https://github.com/blindsidenetworks/scalelite/blob/master/docker-README.md; NGINX is handled on the host, to have control over authentication.

```
apt-get install apt-transport-https ca-certificates curl software-properties-common gpg-agent
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
apt-get update
apt-get install docker-ce
docker network create scalelite
```

## Nginx
```
lb.bbb.tbm.tudelft.nl system # apt-get install certbot nginx python3-certbot-nginx
```

Place files in /etc/nginx/conf.d/ and /etc/bigbluebutton/nginx, replace default config in /etc/nginx/sites-enabled/default

Adjust paths in /etc/bigbluebutton/nginx/ to point to /srv/nfsroot/recordings/var/bigbluebutton/published

Comment out auth_req related statements for now
```
# service nginx restart
# lb.bbb.tbm.tudelft.nl nginx # certbot --nginx -d lb.bbb.tbm.tudelft.nl
lb.bbb.tbm.tudelft.nl nginx # openssl dhparam -out /etc/nginx/dhparam.pem 2048
```

Restore configuration from before certbot (with ssl in there) and restart nginx

## Adding BBB Backends
Secrets come from `bbb-conf --secret` on the hosts.
```
lb.bbb.tbm.tudelft.nl nginx # docker exec -it scalelite-api /bin/sh
~ $ ./bin/rake servers:add[https://vcr000.bbb.tbm.tudelft.nl/bigbluebutton/api/,<secret_removed_for_documentation>,1]
OK
id: c1c68f97-cc21-40b1-82d9-36a416c3d436
~ $ ./bin/rake servers:add[https://vcr001.bbb.tbm.tudelft.nl/bigbluebutton/api/,<removed_for_documentation>,1]
OK
id: b9f62e9c-6da2-4dee-85aa-032eed021907
~ $ ./bin/rake servers:enable[c1c68f97-cc21-40b1-82d9-36a416c3d436]
OK
~ $ ./bin/rake servers:enable[b9f62e9c-6da2-4dee-85aa-032eed021907]
OK
```

Checking status of the LB
```
~ $ ./bin/rake status
          HOSTNAME           STATE   STATUS  MEETINGS  USERS  LARGEST MEETING  VIDEOS 
 vcr000.bbb.tbm.tudelft.nl  enabled  online         0      0                0       0 
 vcr001.bbb.tbm.tudelft.nl  enabled  online         0      0                0       0
```

## Getting LB Logs
```
# docker logs $containername
```
Containernames can be found with:
```
# docker ps
```

# Creation of download links
We encode a downloadable video on the hosts. For these, a presentation format has to be added if they are present. The following script takes care of that.
```
lb.bbb.tbm.tudelft.nl download-link-creater # apt-get install python3-psycopg2
```

Create /opt/bbb-tools/create_dl_link.py
```
#!/usr/bin/python3
import os
import re
import sys

import psycopg2
conn = psycopg2.connect("dbname=scalelite user=scalelite password=<redacted_for_documentation> host=10.23.42.1")

def create_download_link(meetingid):
#	print("Getting "+meetingid)
	cur = conn.cursor()
	cur.execute("SELECT id FROM recordings WHERE record_id = %s;", (meetingid,))
	recordingid = cur.fetchall()
	if not recordingid:
#		print('No record found')
		sys.exit(0)
	else:
		cur.execute("SELECT * FROM playback_formats WHERE format = 'download' AND recording_id = %s;", (recordingid[0][0],))
		download = cur.fetchall()
		if not download:
			cur.execute("SELECT length FROM playback_formats WHERE format = 'presentation' AND recording_id = %s;", (recordingid[0][0],))
			length = cur.fetchall()
			cur.execute("INSERT INTO playback_formats (recording_id, format, url, length) VALUES (%s, 'download', %s, %s);", (recordingid[0][0],"/presentation/"+meetingid+"/video.mp4",length[0][0]))
			conn.commit()
#		else:
#			print("record exists; Exiting")

def check_video_mp4(meetingid):
	f = "/srv/nfsroot/recordings/var/bigbluebutton/published/presentation/"+meetingid+"/video.mp4"
	if os.path.isfile(f):
		return True
	else:
		return False

def get_meeting_ids():
	cur = conn.cursor()
	cur.execute("SELECT record_id FROM recordings")
	meetings = cur.fetchall()
	
	for m in meetings:
		if m:
			if check_video_mp4(m[0]):
				create_download_link(m[0])


get_meeting_ids()
```

Add cronjob to execute this every 5min
```
# cat /etc/cron.d/bbb-download-link 
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

*/5 * * * * root /opt/bbb-tools/create_dl_link.py 
```

# Install bbb-rec-perm

This is an additional authentication hook that ensures recordings are only publicly accessible if a user wants that. 

```
# apt-get install fcgiwrap
```

Follow documentation here: https://github.com/ichdasich/bbb-rec-perm

# Munin

Copy bbb-lb scripts to /etc/munin/plugins/

Add
```
[bbb-lb*]
user root
```
to `/etc/munin/plugin-conf.d/muin-node`

# backup

Create `/srv/backups/lb.bbb.tbm.tudelft.nl`

place backup file into /etc/cron.daily/

```
#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:

echo "Backup on local machine"
rsync -a /etc /srv/nfsroot/backups/lb.bbb.tbm.tudelft.nl
sudo -u postgres pg_dumpall > /srv/nfsroot/backups/lb.bbb.tbm.tudelft.nl/postgres_dump.sql


echo "Waiting for backup on vcr000"
until [ -f /srv/nfsroot/backups/vcr000.bbb.tbm.tudelft.nl/DONE ]
do
     sleep 5
done

echo "Waiting for backup on vcr001"
until [ -f /srv/nfsroot/backups/vcr001.bbb.tbm.tudelft.nl/DONE ]
do
     sleep 5
done

echo "Waiting for backup on frontend"
until [ -f /srv/nfsroot/backups/frontend.bbb.tbm.tudelft.nl/DONE ]
do
     sleep 5
done

rm -f /srv/nfsroot/backups/vcr000.bbb.tbm.tudelft.nl/DONE
rm -f /srv/nfsroot/backups/vcr001.bbb.tbm.tudelft.nl/DONE
rm -f /srv/nfsroot/backups/frontend.bbb.tbm.tudelft.nl/DONE

backupfile="/srv/nfsroot/backups/upload/week/`date +%s`-backup.tar.enc"

tar cf - /srv/nfsroot/backups/lb.bbb.tbm.tudelft.nl/ /srv/nfsroot/backups/vcr001.bbb.tbm.tudelft.nl/ /srv/nfsroot/backups/vcr000.bbb.tbm.tudelft.nl/ /srv/nfsroot/backups/frontend.bbb.tbm.tudelft.nl/ /srv/nfsroot/recordings/ | openssl enc -e -aes256 -pbkdf2 -k '<redacted_for_documentation>' -out $backupfile

find /srv/nfsroot/backups/upload/week/ -type f -mtime +7 -exec rm -f -- '{}' \;

#dom=`date '+%d'`
#mon=`date '+%m'`
#
#if [ $dom == 01 ]
#then
#        cp $backupfile /srv/nfsroot/backups/upload/month
#        if [ $mon == 01 ]
#        then
#                cp $backupfile /srv/nfsroot/backups/upload/year
#        fi
#fi
rsync -av --delete -e 'ssh -p23' /srv/nfsroot/backups/upload/ u244865@u244865.your-storagebox.de:backups/
```

## Decryption of backup

```
cat 1602863774-backup.tar.enc| openssl enc -d -aes256 -pbkdf2 -k "<redacted>" -out - | tar xvf - -C /tmp
```

# monitoring recording processing
In case of an error in recording processing, these are not imported. Place `/opt/monitoring/check_recording_import` to check for stale files.

```
#!/bin/bash

FILES=`find $@ -mmin +5 -type f|wc -l`

if [ "$FILES" == "0" ];
then
        echo "OK: No stale recordings"
else
        echo "CRITICAL: $FILES unimported recordings older than 5 minutes"
fi;
```
