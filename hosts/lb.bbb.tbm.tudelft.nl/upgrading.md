# Upgrading the base system



* Check if nagios is green. [Nagios4]( https://mgmt.bbb.tbm.tudelft.nl/nagios4/ )

* Before doing any upgrade, [check first if there are running rooms](https://bbb.tbm.tudelft.nl/b/admins/rooms). Do NOT upgrade docker packages while there are active users on the platform.
  + That's because the load balancer (scalelite) is run as docker container and thus a docker upgrade could cause service interruption.

```
apt-get update; apt-get dist-upgrade
```

# Upgrading scalelite-docker
The docker containers are stateless; If the service is restarted and a newer version is available upstream, they get automatically updated, i.e., the new image is being pulled.

```
systemctl restart scalelite.target
```
If a databasetable is missing, recreate it with:
```
sudo docker exec -t scalelite-api bundle exec rake db:migrate
```
