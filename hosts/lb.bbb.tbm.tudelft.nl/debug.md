# General Debug

# Monitoring Debug
This section lists the monitoring scripts running for this host and details their debug pathways.

## APT update status
### Description
Goes to CRITICAL for security updates, and to WARNING for feature updates;

### Debug and Resolution
Update the host.

## BBB API on LB

### Description
Checks whether the BBB API successfully returns on a getRecordings request.

### Debug and Resolution
Check whether the postgres DB is available. If it is, verify if at least one worker is up. Consult the logs of the bbb-api docker container.

## Check Postgres Availability

### Desciption
Checks whether the scalelite DB is available in postgres.

### Debug and Resolution

Restart postgres, check logs. An issue encountered before was that the DB was encoded in a locale that was not available on the system.

## Check TLS Certificate Status
### Desciption
Checks certificate status for bbb.tbm.tudelft.nl Turns to WARNING 28 days before expirery, and to CRITICAL 14 days before expirery.

### Debug and Resolution
Request a new certificate from TU Delft central IT.

## Check for active PSQL connection in recording importer
### Description
Checks if there is an active PSQL connection in the API container (does not automatically reconnect after it died, see https://github.com/blindsidenetworks/scalelite/issues/239

### Debug and Resolution
Restart the container: `systemctl restart scalelite-recording-importer.service`

## Check for recordings without a downloadable video
### Description
Checks if there are any videos where there is no combined video.mp4 in the recording. 


### Debug and Resolution
This happens if the video-encoding post-processing on the workers fails. Check metadata.xml in the offending recordings' directory to see on which worker it was processed. Check `/var/log/bigbluebutton/bbb-rap-worker.log` and `/var/log/bigbluebutton/post_processing.log` on that worker for further information.

Use vcr000 and /opt/bbb-mt to reencode the video, by exporting the recordingsdir RW on lb., mounting it on vcr000 and reprocessing the recording. Do not forget to revert these config changes.

## Check for stale recordings
### Description
This check verifies that the recording-importer on the loadbalancer imports videos. Critical if it finds at least one recording in the spool older than 5 minutes.

### Debug and Resolution
Investigate the system's logs.

Restart the recording-importer docker containers (`systemctl restart bbb-recording-importer.target`).

## Check forward DNS

### Descirption
This check ensures that lb.bbb.tbm.tudelft.nl points to the host's IPv4 address.

### Debug and Resolution
Verify against the NS of tudelft.nl that they do return the correct names. If not, contact central to get the names fixed.

## Check lb.bbb.surfcloud.nl TLS Certificate Status
### Desciption
Checks certificate status for lb.bbb.tbm.tudelft.nl Turns to WARNING 28 days before expirery, and to CRITICAL 14 days before expirery.

### Debug and Resolution
This certificate comes from Let's Encrypt. Check that certbot is able to renew the certificate.

## Check network interface bandwidth utilization

### Descirption
Checks utilization of the network interface. Warning if more than 80%, critical if more than 90% bandwidth are used.

### Debug and Resolution
Except for the workers, this should not happen. Might occur occasionally. If this happens on one of the workers due to a large meeting, cross check meeting sizes at that time in munin and upgrade to a 10gE interface for all workers.

## Check reverse DNS

### Description
Checks if the reverse DNS of the host matches its name.

### Debug and Resolution
Check that the correct name is being set in the Hetzner interface.

## HTTPS

### Description
Checks if the webserver is running on port 443.

### Debug and Resolution
Check if nginx is running, and if not, investigate why in the logs:

```
systemctl status nginx
jornalctl -xn
```

For debugging, access/error logs might have to be enabled. Comment out the log redirection to /dev/null in /etc/nginx/nginx.conf for this.

## MD RAID Status Check

### Descirption
Checks if all arrays in the system are in sync. WARNING if the RAID resyncs, CRITICAL if it failed.

### Debug and Resolution
Identify the serial number of the affected device using `smartctl` and schedule a replacement with Hetzner via the webinterface.

## Root Partition

### Description
Checks utilization of the root partition. Warning if less than 20% are free, critical, if less than 10% are free.

### Debug and Resolution
As root has ~1TB, this should not be an issue any time soon. If it occurs, schedule for an increase in disk-size. Keep in mind that more than 1tb of recording storage will lead to issues with the backups, see below. 

## Mount at /srv/nfsroot/backups
### Description
Checks utilization of the backup partition. Warning if less than 20% are free, critical, if less than 10% are free.

### Debug and Resolution
This may happen as soon as the average daily backups reaches ~1.1TB in size. At this point, the backup strategy will have to be adjusted, i.e., we must go for incremental Backups. An upgrade of the main network interface might be beneficial as well if the backups reach this size.

## SSH
### Description 
Checks availability of SSH.

### Debug and Resolution
Order a remote console from hetzner and check VTY for errors.

