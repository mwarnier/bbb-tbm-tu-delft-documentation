# lb.bbb.tbm.tudelft.nl
```
IPv4: 116.202.55.73
IPv6: 2a01:4f8:10b:43b0::2
IPv4 Backend (vid 4000): 10.23.42.1
Location: Falkenstein, DE
Purpose: Loadbalancer and Recordings Storage
Services: psql, nfs, nginx, docker (for scalelite)
OS: Ubuntu 20.04
Comment: 2x 1TB NVMe RAID1 for / (Production Storage), 2x 8TB RAID1 Spinning for /srv/nfsroot/backups (pre-creation of backup files)
```
