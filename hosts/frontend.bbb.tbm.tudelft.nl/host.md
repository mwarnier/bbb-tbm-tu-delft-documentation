# frontend.bbb.tbm.tudelft.nl
```
IPv4: 88.99.57.235
IPv6: 2a01:4f8:10a:a2a::2
IPv4 Backend (vid 4000): 10.23.42.2
Location: Falkenstein, DE
Purpose: Frontends for bbb. and dev.bbb.
Services: psql, nginx, docker (for greenlight)
OS: Ubuntu 20.04
Comment: -
```
