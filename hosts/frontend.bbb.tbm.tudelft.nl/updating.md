# Base System

For the most part, the system can be updated using `apt-get update ; apt-get dist-upgrade'

# Updating the application

## Creating new release

There is a git repository with a greenlight fork at https://github.com/ichdasich/greenlight

With this cloned on your local dev machine:

Get the newest upstream version:
```
greenlight-dev $ git fetch --all
```

Create a new local branch for the newest upstream version:

```
git checkout tags/release-2.9.0 -b upstream-2.9.0
```

Create custom merge-target for TU Delft version of greenlight:

```
git checkout -b custom-v290
```

Update the feature branches:

Update the branch adding https://bbb.tbm.tudelft.nl/b/legal
```
git checkout legal
git merge upstream-2.9.0
```

Update the branch adding custom permissions for recordings
```
git checkout rec_restrictions
git merge upstream-2.9.0
```

Update the branch adding customization for OpenID authentication
```
git checkout openid-changes
git merge upstream-2.9.0
```

Update the branch adding visual changes and improvements that are TU Delft specific
```
git checkout tud_visual
git merge upstream-2.9.0
```

Update the branch adding streaming:
```
git checkout streaming
git merge upstream-2.9.0
```

Merge all feature branches into the new custom-branch
```
git checkout custom-v290
git merge legal
git merge rec_restrictions
git merge openid-changes
git merge tud_visual
git merge streaming
```

Push the new custom branch to github
```
git push origin custom-v290
```

## Updating docker images
On frontend, sudo and go to `/srv/greenlight-surf-dev`:

Update to latest and create a new local branch:
```
git fetch --all
git checkout origin/custom-v290 -b custom-v290
```

Build dev image:
```
/srv/greenlight-surf-dev/srv/greenlight-surf-dev # ./scripts/image_build.sh surf/greenlight-dev release-v2
```

Restart the dev-container to ensure everyting is working. Note that the restart takes some time.

```
/srv/greenlight-surf-dev/srv/greenlight-surf-dev # docker-compose down ; docker-compose up -d
```

Go to https://dev.bbb.tbm.tudelft.nl/ and check if all features are working:

- Check if you can log in
- Check if you can start/stop rooms
- Check if you can set room settings (recording permission!) and if this is reflected in started rooms
- Check if SAML auth works (user: acase / pass: <redacted for documentation> against the TU Delft test realm)
- Check if custom slide upload works

If the release works, build a new container for production:

Build prod image:
```
/srv/greenlight-surf-dev/srv/greenlight-surf-dev # ./scripts/image_build.sh surf/greenlight-prod release-v2
```

Then restart the production container:
```
cd /srv/greenlight
docker-compose down
docker-compose up -d
```

Wait for prod to come back, then run all tests above on prod. Due to the downtime a restart incurs, please to this out-of-production time. 
