# General Debug


# Monitoring Debug
This section lists the monitoring scripts running for this host and details their debug pathways.

## APT update status
### Description
Goes to CRITICAL for security updates, and to WARNING for feature updates; 

### Debug and Resolution
Update the host.

## Check Dev Frontend Availability

### Description
Does a https request to https://dev.bbb.tbm.tudelft.nl/b; This fails if the frontend is down.

### Debug and Resolution
The two most common cases that might occur here are that nginx is not running on frontend, or that the docker containers for the host are not running.

#### nginx
Check if nginx is running, and if not, investigate why in the logs:

```
systemctl status nginx
jornalctl -xn
```

For debugging, access/error logs might have to be enabled. Comment out the log redirection to /dev/null in /etc/nginx/nginx.conf for this.

#### docker
Check whether the container runs with 
```
docker ps
```
If it does not run, try to start it with:
```
cd /srv/greenlight-surf-dev
docker-compose up -d
```

You can view the logs with
```
docker log $containername
```

## Check Frontend Availability
Does a https request to https://bbb.tbm.tudelft.nl/b; This fails if the frontend is down.

### Debug and Resolution
The two most common cases that might occur here are that nginx is not running on frontend, or that the docker containers for the host are not running.

#### nginx
Check if nginx is running, and if not, investigate why in the logs:

```
systemctl status nginx
jornalctl -xn
```

For debugging, access/error logs might have to be enabled. Comment out the log redirection to /dev/null in /etc/nginx/nginx.conf for this.

#### docker
Check whether the container runs with 
```
docker ps
```
If it does not run, try to start it with:
```
cd /srv/greenlight
docker-compose up -d
```

You can view the logs with
```
docker log $containername
```

## Check Mailing via Docker
### Description
This checks whether the docker container running the greenlight frontend is able to connect (tcp) to the mailserver on frontend, and receives a banner.

### Debug and Resolution
If this fails, most likely either the mailserver is stopped, or firewalling is having issues.

Check if postfix runs and inspect `/var/log/mail.log`. Also check `iptables -L -v -n` for rules that may deny RFC1918 pools to connect.


## Check TLS Certificate Status
### Desciption
Checks certificate status for bbb.tbm.tudelft.nl Turns to WARNING 28 days before expirery, and to CRITICAL 14 days before expirery.

### Debug and Resolution
Request a new certificate from TU Delft central IT.

## Check bbb.surfcloud.nl TLS Certificate Status
### Desciption
Checks certificate status for bbb.tbm.tudelft.nl Turns to WARNING 28 days before expirery, and to CRITICAL 14 days before expirery.

### Debug and Resolution
This certificate comes from Let's Encrypt. Check that certbot is able to renew the certificate.

## Check dev.bbb TLS Certificate Status
### Desciption
Checks certificate status for bbb.tbm.tudelft.nl Turns to WARNING 28 days before expirery, and to CRITICAL 14 days before expirery.

### Debug and Resolution
Request a new certificate from TU Delft central IT.

## Check for email relaying via TU Delft

### Description
This check is passive. It is updated when the monitoring system receives an email relayed via TU Delft's mail servers (send via 'Send monitoring mails for check_mail_delivery' below).

### Debug and Resolution
If this fails, inspect the mailq on the host and investigate whether manual relaying via smtp-a.tudelft.nl is possible.

## Check forward DNS

### Descirption 
This check ensures that frontend.bbb.tbm.tudelft.nl points to the host's IPv4 address.

### Debug and Resolution
Verify against the NS of tudelft.nl that they do return the correct names. If not, contact central to get the names fixed.

## Check frontend DNS

### Descirption 
This check ensures that bbb.tbm.tudelft.nl points to the host's IPv4 address.

### Debug and Resolution
Verify against the NS of tudelft.nl that they do return the correct names. If not, contact central to get the names fixed.

## Check frontend dev DNS

### Descirption 
This check ensures that dev.bbb.tbm.tudelft.nl points to the host's IPv4 address.

### Debug and Resolution
Verify against the NS of tudelft.nl that they do return the correct names. If not, contact central to get the names fixed.

## Check network interface bandwidth utilization

### Descirption
Checks utilization of the network interface. Warning if more than 80%, critical if more than 90% bandwidth are used.

### Debug and Resolution
Except for the workers, this should not happen. Might occur occasionally. If this happens on one of the workers due to a large meeting, cross check meeting sizes at that time in munin and upgrade to a 10gE interface for all workers.

## Check reverse DNS

### Description
Checks if the reverse DNS of the host matches its name.

### Debug and Resolution
Check that the correct name is being set in the Hetzner interface.

## HTTPS

### Description
Checks if the webserver is running on port 443.

### Debug and Resolution
Check if nginx is running, and if not, investigate why in the logs:

```
systemctl status nginx
jornalctl -xn
```

For debugging, access/error logs might have to be enabled. Comment out the log redirection to /dev/null in /etc/nginx/nginx.conf for this.

## MD RAID Status Check

### Descirption
Checks if all arrays in the system are in sync. WARNING if the RAID resyncs, CRITICAL if it failed.

### Debug and Resolution
Identify the serial number of the affected device using `smartctl` and schedule a replacement with Hetzner via the webinterface.

## Root Partition

### Description
Checks utilization of the root partition. Warning if less than 20% are free, critical, if less than 10% are free.

### Debug and Resolution
As root has ~500gb, this should never be an issue. If it still becomes one, it is most likely related to old docker images. Clean those up.

## SSH
### Description 
Checks availability of SSH.

### Debug and Resolution
Order a remote console from hetzner and check VTY for errors.

## Send monitoring mails for check_mail_delivery

### Description 
Always OK; Sends an email for the passive mail-sending check.
