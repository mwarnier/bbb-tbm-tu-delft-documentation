# Install docker
```
apt-get install apt-transport-https ca-certificates curl software-properties-common gpg-agent
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
apt-get update
apt-get install docker-ce docker-compose
```

# nginx
```
# apt-get install certbot nginx python3-certbot-nginx
# certbot --nginx -d dev.bbb.tbm.tudelft.nl
# certbot --nginx -d bbb.tbm.tudelft.nl
# openssl dhparam -out /etc/nginx/dhparam.pem 2048
```

## Deploy nginx configuration
```
server {
  listen 80 default_server;
  listen [::]:80 default_server;

  root /var/www/html;

  # Add index.php to the list if you are using PHP
  index index.html index.htm index.nginx-debian.html;

  server_name _;

  location / {
    try_files $uri $uri/ =404;
  }
}


server {
  root /var/www/html;

  
  index index.html index.htm index.nginx-debian.html;
        server_name dev.bbb.tbm.tudelft.nl; # managed by Certbot

  location / {
    try_files $uri $uri/ =404;
  }

    listen [::]:443 ssl; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/dev.bbb.tbm.tudelft.nl/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/dev.bbb.tbm.tudelft.nl/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

  location /b {
    proxy_pass          http://127.0.0.1:5001;
    proxy_set_header    Host              $host;
    proxy_set_header    X-Forwarded-For   $proxy_add_x_forwarded_for;
    proxy_set_header    X-Forwarded-Proto $scheme;
    proxy_http_version  1.1;
  }
  
  location /b/cable {
    proxy_pass          http://127.0.0.1:5001;
    proxy_set_header    Host              $host;
    proxy_set_header    X-Forwarded-For   $proxy_add_x_forwarded_for;
    proxy_set_header    X-Forwarded-Proto $scheme;
    proxy_set_header    Upgrade           $http_upgrade;
    proxy_set_header    Connection        "Upgrade";
    proxy_http_version  1.1;
    proxy_read_timeout  6h;
    proxy_send_timeout  6h;
    client_body_timeout 6h;
    send_timeout        6h;
  }
  # Allow larger body size for uploading presentations
  location ~ /preupload_presentation$ {
    client_max_body_size 30m;
  
    proxy_pass          http://127.0.0.1:5001;
    proxy_set_header    Host              $host;
    proxy_set_header    X-Forwarded-For   $proxy_add_x_forwarded_for;
    proxy_set_header    X-Forwarded-Proto $scheme;
    proxy_http_version  1.1;
  }
  
  location = / {
    return 307 /b;
  }
  
  location /rails/active_storage {
    return 301 /b$request_uri;
  }

}


server {
  root /var/www/html;

  
  index index.html index.htm index.nginx-debian.html;
        server_name bbb.tbm.tudelft.nl; # managed by Certbot

  location / {
    try_files $uri $uri/ =404;
  }

    listen [::]:443 ssl; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/bbb.tbm.tudelft.nl/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/bbb.tbm.tudelft.nl/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

  location /b {
    proxy_pass          http://127.0.0.1:5000;
    proxy_set_header    Host              $host;
    proxy_set_header    X-Forwarded-For   $proxy_add_x_forwarded_for;
    proxy_set_header    X-Forwarded-Proto $scheme;
    proxy_http_version  1.1;
  }
  
  location /b/cable {
    proxy_pass          http://127.0.0.1:5000;
    proxy_set_header    Host              $host;
    proxy_set_header    X-Forwarded-For   $proxy_add_x_forwarded_for;
    proxy_set_header    X-Forwarded-Proto $scheme;
    proxy_set_header    Upgrade           $http_upgrade;
    proxy_set_header    Connection        "Upgrade";
    proxy_http_version  1.1;
    proxy_read_timeout  6h;
    proxy_send_timeout  6h;
    client_body_timeout 6h;
    send_timeout        6h;
  }
  
  # Allow larger body size for uploading presentations
  location ~ /preupload_presentation$ {
    client_max_body_size 30m;
  
    proxy_pass          http://127.0.0.1:5000;
    proxy_set_header    Host              $host;
    proxy_set_header    X-Forwarded-For   $proxy_add_x_forwarded_for;
    proxy_set_header    X-Forwarded-Proto $scheme;
    proxy_http_version  1.1;
  }
  location = / {
    return 307 /b;
  }
  location /rails/active_storage {
    return 301 /b$request_uri;
  }

}


server {
    if ($host = bbb.tbm.tudelft.nl) {
        return 301 https://$host$request_uri;
    } # managed by Certbot

  listen 80 ;
  listen [::]:80 ;
    server_name bbb.tbm.tudelft.nl;
    return 404; # managed by Certbot


}
server {
    if ($host = dev.bbb.tbm.tudelft.nl) {
        return 301 https://$host$request_uri;
    } # managed by Certbot

  listen 80 ;
  listen [::]:80 ;
    server_name dev.bbb.tbm.tudelft.nl;
    return 404; # managed by Certbot


}
```

# Psql
```
# apt-get install postgresql postgresql-contrib
lb.bbb.tbm.tudelft.nl ~ # sudo -u postgres createuser --interactive
Enter name of role to add: greenlight
Shall the new role be a superuser? (y/n) y
lb.bbb.tbm.tudelft.nl ~ # sudo -u postgres createdb greenlight
lb.bbb.tbm.tudelft.nl ~ # sudo -u postgres psql
psql (12.4 (Ubuntu 12.4-0ubuntu0.20.04.1))
Type "help" for help.

postgres=# \password postgres
Enter new password:
Enter it again:
postgres=# \password greenlight
Enter new password:
Enter it again:
postgres=# \q
```

Change in /etc/postgresql/12/main/hba.conf
```
# IPv4 backend connections:                                                     
host    all       greenlight       172.16.0.0/12           md5                   
host    all       greenlight       10.0.0.0/8           md5                      
host    all       greenlight       192.168.0.0/16           md5   
```

Change in /etc/postgresql/12/main/postgresql.conf 
```
listen_addresses = '10.23.42.2'
```

# Installing Application

Copy over docker folders to /srv/

Build images:
```
git pull ; ./scripts/image_build.sh surf/greenlight-prod release-v2 ; 
git pull ; ./scripts/image_build.sh surf/greenlight-dev release-v2 ;
```

# ferm
```
# -*- shell-script -*-
#
# /etc/ferm/ferm.conf

# Default rules
domain (ip ip6) {
    table filter {
        # Default Policies
        chain INPUT policy DROP;
        chain OUTPUT policy ACCEPT;

        # loopback traffic
        chain INPUT interface lo ACCEPT;
        chain OUTPUT outerface lo ACCEPT;

        chain (INPUT OUTPUT) {
            # ICMP is very handy and necessary
            proto icmp ACCEPT;

            # connection tracking
            mod conntrack ctstate (RELATED ESTABLISHED) ACCEPT;
        }
    }
}

domain (ip ip6) {
    table filter {
        chain (DOCKER DOCKER-USER DOCKER-INGRESS DOCKER-ISOLATION-STAGE-1 DOCKER-ISOLATION-STAGE-2 FORWARD) @preserve;
    }

    table nat {
        chain (DOCKER DOCKER-INGRESS PREROUTING OUTPUT POSTROUTING) @preserve;
    }
}


domain (ip ip6) {
    table filter chain INPUT proto tcp dport 22 ACCEPT;
    table filter chain INPUT interface enp35s0.4000 ACCEPT;
}
domain (ip) {
    table filter chain INPUT proto tcp dport 80 ACCEPT;
    table filter chain INPUT proto tcp dport 443 ACCEPT;
}
domain (ip6) {
    table filter chain INPUT proto tcp dport 80 ACCEPT;
    table filter chain INPUT proto tcp dport 443 ACCEPT;
}
domain (ip) {
    table filter chain INPUT {
        saddr (95.217.131.128/32) proto (tcp) dport 4949 ACCEPT;
        saddr (172.16.0.0/12 10.0.0.0/8 192.168.0.0/16) proto (tcp) dport 25 ACCEPT;
        saddr (172.16.0.0/12 10.0.0.0/8 192.168.0.0/16) proto (tcp) dport 5432 ACCEPT;
        proto tcp dport 4949 REJECT;
    }
}
```

# postfix
Greenlight does not support sending mails via port 465, which is our only option; Hence, we set up a postfix relay on the frontend.
```
apt-get install libsasl2-modules postfix mailutils
```

Create `/etc/postfix/sasl_passwd`
```
[mail.isp.example]:587 username:password
```
Generate postmap.
```
# postmap /etc/postfix/sasl_passwd
frontend.bbb.tbm.tudelft.nl log # sudo chown root:root /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.db
frontend.bbb.tbm.tudelft.nl log # sudo chmod 0600 /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.db
```

Create `/etc/postfix/main.cf`

```
relayhost = [smtp-a.tudelft.nl]:465
# enable SASL authentication
smtp_sasl_auth_enable = yes
# disallow methods that allow anonymous authentication.
smtp_sasl_security_options = noanonymous
# where to find sasl_passwd
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
# Enable STARTTLS encryption
smtp_use_tls = yes
smtp_tls_wrappermode = yes
smtp_tls_security_level = encrypt

mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 192.168.0.0/16 10.0.0.0/8 172.16.0.0/12
inet_interfaces = all
inet_protocols = all
```

# Backup
```
apt-get install nfs-client
```
Create nfs mount 
```
mkdir /srv/backup
```
Add to `/etc/fstab`
```
10.23.42.1:/srv/nfsroot/backups/frontend.bbb.tbm.tudelft.nl   /srv/backup nfs defaults 0 0
```

Place `/etc/cron.daily/backup`
```
#!/bin/bash
rsync -a /etc /srv/backup
rsync -a /srv/greenlight /srv/backup
rsync -a /srv/greenlight-surf-dev /srv/backup

touch /srv/backup/DONE
```

# Streaming
Follow https://github.com/ichdasich/bbb-stream-control
