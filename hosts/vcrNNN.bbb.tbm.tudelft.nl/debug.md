# General Debug


# Monitoring Debug
This section lists the monitoring scripts running for this host and details their debug pathways.


## BBB API on host

### Description
Checks whether the BBB API successfully returns on a getMeetings request.

### Debug and Resolution
This is critical; Take the server out of the LB rotation before debug.

Check bbb status using `bbb-conf --status` and `bbb-conf --check`. In case of doubt, restart (`bbb-conf --restart`) or reboot the system. Investigate logs in `/var/log/(bbb-*|bigbluebutton)/*log`.

## BBB webclient on host

### Description
Checks whether the BBB webclient endpoint successfully returns.

### Debug and Resolution
This is critical; Take the server out of the LB rotation before debug.

Check bbb status using `bbb-conf --status` and `bbb-conf --check`. In case of doubt, restart (`bbb-conf --restart`) or reboot the system. Investigate logs in `/var/log/(bbb-*|bigbluebutton)/*log`.

## APT update status
### Description
Goes to CRITICAL for security updates, and to WARNING for feature updates; 

### Debug and Resolution
Update the host.

## Check TLS Certificate Status
### Desciption
Checks certificate status for vcrNNN.bbb.tbm.tudelft.nl Turns to WARNING 28 days before expirery, and to CRITICAL 14 days before expirery.

### Debug and Resolution
Request a new certificate from TU Delft central IT.

## Check forward DNS

### Descirption 
This check ensures that vcrNNN.bbb.tbm.tudelft.nl points to the host's IPv4 address.

### Debug and Resolution
Verify against the NS of tudelft.nl that they do return the correct names. If not, contact central to get the names fixed.

## Check network interface bandwidth utilization

### Descirption
Checks utilization of the network interface. Warning if more than 80%, critical if more than 90% bandwidth are used.

### Debug and Resolution
Except for the workers, this should not happen. Might occur occasionally. If this happens on one of the workers due to a large meeting, cross check meeting sizes at that time in munin and upgrade to a 10gE interface for all workers.

## Check reverse DNS

### Description
Checks if the reverse DNS of the host matches its name.

### Debug and Resolution
Check that the correct name is being set in the Hetzner interface.

## HTTPS

### Description
Checks if the webserver is running on port 443.

### Debug and Resolution
Check if nginx is running, and if not, investigate why in the logs:

```
systemctl nginx apache
jornalctl -xn
```

## Various mountpoints (ramdisks)

### Description
To prevent meeting data from touching the SSDs, temporary directories used by BBB are written to ramdisks. 
This check the utilization of each. Warning if less than 20% are free, critical, if less than 10% are free.

### Debug and Resolution
These should be sufficiently sized to hold ~1 week of non-stop meeting data. If they run full, something is wrong. Investigate.

## Root Partition

### Description
Checks utilization of the root partition. Warning if less than 20% are free, critical, if less than 10% are free.

### Debug and Resolution
This can occur if logging consumes too much space, or files are cached in the wrong place. In any case: investigate.

## SSH
### Description
Checks availability of SSH.


