# Additional Software
Install:
- ferm
- nfs-client
- tcpdump
# ferm
Due to the docker integration of our video processing feature, we are using a (manually) Installed ferm from Ubuntu 20.04! In case of an upgrade of ferm, /usr/sbin/ferm has to be replaced with one from a 20.04 host. This will hopefully go away with BBB 2.3 on Ubuntu 18.04.

```
# -*- shell-script -*-
#
#  Configuration file for ferm(1).
#
domain (ip ip6) {
        table filter {
            chain INPUT {
                policy DROP;

                # connection tracking
                mod state state INVALID DROP;
                mod state state (ESTABLISHED RELATED) ACCEPT;

                # allow local packet
                interface lo ACCEPT;

                # respond to ping
                proto icmp ACCEPT; 

                # allow BBB
                proto udp dport 16384:32768 ACCEPT;
                proto tcp dport (80 443) ACCEPT;


                # allow SSH connections
                proto tcp dport ssh ACCEPT;
                interface eth0.4000 ACCEPT;
            }
}
domain (ip ip6) {
    table filter {
        chain (DOCKER DOCKER-USER DOCKER-INGRESS DOCKER-ISOLATION-STAGE-1 DOCKER-ISOLATION-STAGE-2 FORWARD) @preserve;
    }

    table nat {
        chain (DOCKER DOCKER-INGRESS PREROUTING OUTPUT POSTROUTING) @preserve;
    }
}


# allow inbound SIP
domain (ip) {
    table filter chain INPUT {
        saddr (193.169.139.0/24 193.169.138.0/24)
                proto (tcp udp) dport 5060 ACCEPT;
    }
}

domain (ip) {
    table filter chain INPUT {
        saddr (95.217.131.128/32) proto (tcp) dport 4949 ACCEPT;
        proto tcp dport 4949 REJECT;
    }
}

```

# Install BBB
BBB is being installed with an installation script provided upstream. The following command is being used:
```
wget -qO- https://packages-eu.bigbluebutton.org/bbb-install.sh | bash -s -- -v xenial-22 -s vcr000.bbb.tbm.tudelft.nl -e t.fiebig@tudelft.nl -c turn.bbb.tbm.tudelft.nl:<redacted_for_documentation>
```

# fstab
We prevent writing meeting related data to disk for privacy reasons. Hence, several folders are mounted as tmpfs.

Create folders:
```
mkdir -p /mnt/scalelite-recordings/var/bigbluebutton/spool
mkdir -p /var/lib/kurento/recordings/
mkdir -p /var/lib/kurento/screenshare/
```

Write /etc/fstab:
```
proc /proc proc defaults 0 0
/dev/sda1 none swap sw 0 0
/dev/sda2 /boot ext3 defaults 0 0
/dev/sda3 / ext4 defaults 0 0

10.23.42.1:/srv/nfsroot/recordings/var/bigbluebutton/spool   /mnt/scalelite-recordings/var/bigbluebutton/spool nfs defaults 0 0

none            /tmp            tmpfs   defaults        0       0
none            /var/tmp        tmpfs   defaults        0       0

tmpfs /var/bigbluebutton/recording/raw/ tmpfs rw,nosuid,noatime,uid=998,gid=998,size=16G,mode=0755   0    0
tmpfs /var/bigbluebutton/unpublished/ tmpfs rw,nosuid,noatime,uid=998,gid=998,size=16G,mode=0755   0    0
tmpfs /var/bigbluebutton/published/presentation/ tmpfs rw,nosuid,noatime,uid=998,gid=998,size=16G,mode=0755   0    0
tmpfs /usr/share/red5/webapps/video/streams/ tmpfs rw,nosuid,noatime,uid=999,gid=999,size=16G,mode=0755   0    0
tmpfs /usr/share/red5/webapps/screenshare/streams/ tmpfs rw,nosuid,noatime,uid=999,gid=999,size=16G,mode=0755   0    0
tmpfs /usr/share/red5/webapps/video-broadcast/streams/ tmpfs rw,nosuid,noatime,uid=999,gid=999,size=16G,mode=0755   0    0
tmpfs /var/lib/kurento/recordings/ tmpfs rw,nosuid,noatime,uid=114,gid=121,size=16G,mode=0755   0    0
tmpfs /var/lib/kurento/screenshare/ tmpfs rw,nosuid,noatime,uid=114,gid=121,size=16G,mode=0755   0    0
tmpfs /var/freeswitch/meetings/ tmpfs rw,nosuid,noatime,uid=997,gid=997,size=16G,mode=0755   0    0
```

## Scalelite Recording processing
```
vcr001.bbb.tbm.tudelft.nl ~ # mkdir -p /var/bigbluebutton/recording/scalelite
vcr001.bbb.tbm.tudelft.nl ~ # chown -Rv bigbluebutton:bigbluebutton /var/bigbluebutton/recording/scalelite 
```

Changed ownership of `/var/bigbluebutton/recording/scalelite` from `root:root` to `bigbluebutton:bigbluebutton`

Create scalelite spool group:
```
vcr000.bbb.tbm.tudelft.nl ~ # groupadd scalelite-spool
Change gid to 2000 and add user bigbluebutton
```

Add to /etc/sudoers
```
bigbluebutton ALL = NOPASSWD: /usr/bin/bbb-record
bigbluebutton ALL = NOPASSWD: /bin/rm
```

# Move backup configuration
BBB does not have a concept of configuration persistence across updates. Instead, it provides a mechanism to overwrite configuration files during application startup via /etc/bigbluebutton/bbb-conf/apply-config.sh. Copy this over and adjust (documentation pending).

During a restart, this script saves the overwritten configuration files (i.e., upstream) to /etc/bigbluebutton/conf_old/ and commits them to etckeeper.



## Adjust WebRTCEndpoint Config in config_bu

Adjust bbb config (add phone number and welcome message), limit number of user

Change settings.yml:
- number of breakout rooms
- mirror own webcam
- multiple cameras
- enableNetworkInformation
- enable pagination


# Create dialplan (allow SIP call in)
```
root@vcr000:/opt/freeswitch/conf/dialplan/public# cat my_provider.xml 
<extension name="from_my_provider">
 <condition field="destination_number" expression="^31152010935">
   <action application="answer"/>
   <action application="sleep" data="500"/>
   <action application="play_and_get_digits" data="5 5 3 7000 # conference/bbb-surfcloud.wav ivr/ivr-that_was_an_invalid_entry.wav pin \d+"/>
   <action application="transfer" data="SEND_TO_CONFERENCE XML public"/>
 </condition>
</extension>
<extension name="check_if_conference_active">
 <condition field="${conference ${pin} list}" expression="/sofia/g" />
 <condition field="destination_number" expression="^SEND_TO_CONFERENCE$">
   <action application="set" data="bbb_authorized=true"/>
   <action application="transfer" data="${pin} XML default"/>
 </condition>
</extension>
```

Store wav files
```
root@vcr000:/opt/freeswitch/conf/dialplan/public# find / | grep bbb-surfcloud.wav
/opt/freeswitch/share/freeswitch/sounds/en/us/callie/conference/48000/bbb-surfcloud.wav
/opt/freeswitch/share/freeswitch/sounds/en/us/callie/conference/16000/bbb-surfcloud.wav
/opt/freeswitch/share/freeswitch/sounds/en/us/callie/conference/32000/bbb-surfcloud.wav
/opt/freeswitch/share/freeswitch/sounds/en/us/callie/conference/8000/bbb-surfcloud.wav
```

# Create record cleanup script
```
mkdir -p /opt/bbb-tools
```
place del_pres.py in there

## Place cronjob
```
vcr001.bbb.tbm.tudelft.nl bigbluebutton # cat /etc/cron.d/bbb-rec-del 
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

*/5 * * * * root /opt/bbb-tools/del_pres.py 
```

# Video Processing
Use: https://github.com/tilmanmoser/bbb-video-download
```
cd /opt
git clone https://github.com/tilmanmoser/bbb-video-download.git
cd bbb-video-download
```
create tmp folder:
```
vcr001.bbb.tbm.tudelft.nl bbb-video-download # mkdir /opt/bbb-video-download/tmp
```
create tmp mount:
```
tmpfs /opt/bbb-video-download/tmp/ tmpfs rw,nosuid,noatime,uid=998,gid=998,size=16G,mode=0755   0    0
mount -a
```

## Install docker etc. 
```
apt-get install apt-transport-https ca-certificates curl software-properties-common gpg-agent
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable"
apt-get update
apt-get install docker-ce
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose.
chmod +x /usr/local/bin/docker-compose
```
Add bbb to docker group

Follow new install documentation;


# munin
## node-load
The bottleneck of BBB is a single node process that runs single threaded. Hence, we monitor per-node-process CPU utilization.
```
apt-get install python3-psutil
```
place `/etc/munin/plugins/node_load`

```
#!/usr/bin/env python3 
import psutil
import sys
import os
import subprocess

# get all node processes

def get_node_load(config='5'):
        procs = {}
        pids = []
        for proc in psutil.process_iter():
                try:
                        # Get process name & pid from process object.
                        #pInfoDict = proc.as_dict(attrs=['pid', 'name', 'cpu_percent'])
                        pInfoDict = proc.as_dict(attrs=['pid','cmdline'])
                        if pInfoDict['cmdline'][0].split('/')[-1] == 'node':
                                pids.append(pInfoDict)
                except:
                        pass

        process = subprocess.Popen(['/usr/bin/top','-b','-n',config], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        top = stdout.decode('UTF-8').strip().split('\n')
        for proc in pids:
                for t in top:
                        t_items = t.split()
                        if t_items:
                                if t_items[0] == str(proc['pid']):
                                        try:
                                                procs[proc['cmdline'][-1].split('/')[-1]] = t_items[-4]
                                        except:
                                                procs[proc['cmdline'][-1].split('/')[-1]] = '0.0'
        return procs

def print_config():
        print('config bbb-node-load')
        print('graph_title CPU Load per Node Process')
        print('graph_args --base 1000 -l 0')
        print('graph_vlabel CPU Load')
        print('graph_category bigbluebutton')
        print('graph_scale no')
        data = get_node_load(config='1')
        keys = list(data.keys())
        keys.sort()
        for i in keys:
                print(i.replace('.','-')+'.label '+i)
                print(i.replace('.','-')+'.draw AREASTACK')

def print_data():
        data = get_node_load()
        keys = list(data.keys())
        keys.sort()
        for i in keys:
                print(i.replace('.','-')+'.value '+str(data[i]))

if len(sys.argv) > 1:
        if sys.argv[1] == 'config':
                print_config()
else:
        print_data()
```


# backup

```
mkdir /srv/backup
```
add backup to /etc/fstab

Place daily cron job
```
#!/bin/bash

rsync -a /etc /srv/backup
rsync -a /opt /srv/backup

touch /srv/backup/DONE
```

into /etc/cron.daily/backup

```
chmod +x backup
```

# Disable auto-updates for BBB

Disable automatic upgrades for ferm and BBB in `/etc/apt/apt.conf.d/50unattended-upgrades`:

```
Unattended-Upgrade::Package-Blacklist {
        "ferm";
        "bigbluebutton";
        "bbb-apps";
        "bbb-apps-akka";
        "bbb-apps-screenshare";
        "bbb-apps-sip";
        "bbb-apps-video";
        "bbb-apps-video-broadcast";
        "bbb-client";
        "bbb-config";
        "bbb-etherpad";
        "bbb-freeswitch-core";
        "bbb-freeswitch-sounds";
        "bbb-fsesl-akka";
        "bbb-html5";
        "bbb-mkclean";
        "bbb-playback-presentation";
        "bbb-record-core";
        "bbb-red5";
        "bbb-transcode-akka";
        "bbb-web";
        "bbb-webrtc-sfu";
        "bigbluebutton";
        "ffmpeg";
        "libavcodec58:amd64";
        "libavdevice58:amd64";
        "libavfilter7:amd64";
        "libavformat58:amd64";
        "libavresample4:amd64";
        "libavutil56:amd64";
        "libopusenc0";
        "libpostproc55:amd64";
        "libswresample3:amd64";
        "libswscale5:amd64";
```

# Log Retention
In /etc/cron.daily/bigbluebutton:
```
#
# How N days back to keep files
#
history=5
unrecorded_days=7
published_days=7
log_history=7
```

# CPU Freq scheduling
Switch to performance due to audio distortion from freq scaling while audio is being played.
```
update-rc.d ondemand disable

~ # cat /etc/default/cpufrequtils 
### Hetzner Online GmbH - installimage
# cpu frequency scaling
ENABLE="true"
GOVERNOR="performance"
MAX_SPEED="0"
MIN_SPEED="0"

```


