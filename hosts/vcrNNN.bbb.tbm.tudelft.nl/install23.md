# Additional Software
Install:
- ferm
- tcpdump
- zsh
- etckeeper
- dstat
- vlan
- vim
- screen
- nfs-common
- nagios-plugins
- nagios-plugins-contrib

# After-upgrade restore
Restore the following configuration from the old system:
- Hostname
- network
- SSH keys/Certs
- Users/home dirs
- sudo config
- disable root login
- TLS Keys/Certs (after bbb installation)
- `ln -sf /usr/share/zoneinfo/UTC /etc/localtime`

# etckeeper
```
git config --global user.email 'root@localhost'
git config --global user.name 'root'
```

# ferm

```
# -*- shell-script -*-
#
#  Configuration file for ferm(1).
#
domain (ip ip6) {
        table filter {
            chain INPUT {
                policy DROP;

                # connection tracking
                mod state state INVALID DROP;
                mod state state (ESTABLISHED RELATED) ACCEPT;

                # allow local packet
                interface lo ACCEPT;

                # respond to ping
                proto icmp ACCEPT; 

                # allow BBB
                proto udp dport 16384:32768 ACCEPT;
                proto tcp dport (80 443) ACCEPT;


                # allow SSH connections
                proto tcp dport ssh ACCEPT;
                interface enp196s0.4000 ACCEPT;
            }
}
domain (ip ip6) {
    table filter {
        chain (DOCKER DOCKER-USER DOCKER-INGRESS DOCKER-ISOLATION-STAGE-1 DOCKER-ISOLATION-STAGE-2 FORWARD) @preserve;
    }

    table nat {
        chain (DOCKER DOCKER-INGRESS PREROUTING OUTPUT POSTROUTING) @preserve;
    }
}


# allow inbound SIP
domain (ip) {
    table filter chain INPUT {
        saddr (193.169.139.0/24 193.169.138.0/24)
                proto (tcp udp) dport 5060 ACCEPT;
    }
}

domain (ip) {
    table filter chain INPUT {
        saddr (95.217.131.128/32) proto (tcp) dport 4949 ACCEPT;
        proto tcp dport 4949 REJECT;
    }
}

domain (ip) {
    table filter chain OUTPUT {
        outerface enp196s0 daddr (10.0.0.0/8 192.168.0.0/24 172.16.0.0/12) REJECT;
    }
}

```

# Install BBB
BBB is being installed with an installation script provided upstream. The following command is being used:
```
wget -qO- https://ubuntu.bigbluebutton.org/bbb-install.sh | bash -s -- -v bionic-230 -s vcrNNN.bbb.tbm.tudelft.nl -e t.fiebig@tudelft.nl -c turn.bbb.tbm.tudelft.nl:<redacted_for_documentation> -r packages-eu.bigbluebutton.org
```

# fstab
We prevent writing meeting related data to disk for privacy reasons. Hence, several folders are mounted as tmpfs.

Create folders:
```
mkdir -p /mnt/scalelite-recordings/var/bigbluebutton/spool
mkdir -p /var/lib/kurento/recordings/
mkdir -p /var/lib/kurento/screenshare/
```

Write /etc/fstab:

IMPORTANT: Adjust uid/gid values for the system!

```
proc /proc proc defaults 0 0
/dev/sda1 none swap sw 0 0
/dev/sda2 /boot ext3 defaults 0 0
/dev/sda3 / ext4 defaults 0 0

10.23.42.1:/srv/nfsroot/recordings/var/bigbluebutton/spool   /mnt/scalelite-recordings/var/bigbluebutton/spool nfs defaults 0 0

none            /tmp            tmpfs   defaults        0       0
none            /var/tmp        tmpfs   defaults        0       0

tmpfs /var/bigbluebutton/recording/raw/ tmpfs rw,nosuid,noatime,uid=998,gid=998,size=16G,mode=0755   0    0
tmpfs /var/bigbluebutton/unpublished/ tmpfs rw,nosuid,noatime,uid=998,gid=998,size=16G,mode=0755   0    0
tmpfs /var/bigbluebutton/published/presentation/ tmpfs rw,nosuid,noatime,uid=998,gid=998,size=16G,mode=0755   0    0
tmpfs /usr/share/red5/webapps/video/streams/ tmpfs rw,nosuid,noatime,uid=999,gid=999,size=16G,mode=0755   0    0
tmpfs /usr/share/red5/webapps/screenshare/streams/ tmpfs rw,nosuid,noatime,uid=999,gid=999,size=16G,mode=0755   0    0
tmpfs /usr/share/red5/webapps/video-broadcast/streams/ tmpfs rw,nosuid,noatime,uid=999,gid=999,size=16G,mode=0755   0    0
tmpfs /var/lib/kurento/recordings/ tmpfs rw,nosuid,noatime,uid=114,gid=121,size=16G,mode=0755   0    0
tmpfs /var/lib/kurento/screenshare/ tmpfs rw,nosuid,noatime,uid=114,gid=121,size=16G,mode=0755   0    0
tmpfs /var/freeswitch/meetings/ tmpfs rw,nosuid,noatime,uid=997,gid=997,size=16G,mode=0755   0    0
```

## Scalelite Recording processing
```
vcr001.bbb.tbm.tudelft.nl ~ # mkdir -p /var/bigbluebutton/recording/scalelite
vcr001.bbb.tbm.tudelft.nl ~ # chown -Rv bigbluebutton:bigbluebutton /var/bigbluebutton/recording/scalelite 
```

Changed ownership of `/var/bigbluebutton/recording/scalelite` from `root:root` to `bigbluebutton:bigbluebutton`

Create scalelite spool group:
```
vcr000.bbb.tbm.tudelft.nl ~ # groupadd scalelite-spool
Change gid to 2000 and add user bigbluebutton
```

add to `/etc/sudoers`:
```
bigbluebutton ALL = NOPASSWD: /bin/rm
```

# Move backup configuration
BBB does not have a concept of configuration persistence across updates. Instead, it provides a mechanism to overwrite configuration files during application startup via /etc/bigbluebutton/bbb-conf/apply-config.sh. Copy this over and adjust (documentation pending).

During a restart, this script saves the overwritten configuration files (i.e., upstream) to /etc/bigbluebutton/bbb-conf/ and commits them to etckeeper.



## Adjust WebRTCEndpoint Config in config_bu

Adjust bbb config (add phone number and welcome message), limit number of user

Change settings.yml:
- number of breakout rooms
- mirror own webcam
- multiple cameras
- enableNetworkInformation
- enable pagination

# enable multiple recording workers
```
# mkdir -p /etc/systemd/system/bbb-rap-resque-worker.service.d
# cat > override.conf << HERE
[Service]
Environment=COUNT=8
HERE
# systemctl daemon-reload
# systemctl restart bbb-rap-resque-worker.service
# systemctl status bbb-rap-resque-worker.service
● bbb-rap-resque-worker.service - BigBlueButton resque worker for recordings
   Loaded: loaded (/usr/lib/systemd/system/bbb-rap-resque-worker.service; disabled; vendor preset: enabled)
  Drop-In: /etc/systemd/system/bbb-rap-resque-worker.service.d
           └─override.conf
   Active: active (running) since Sat 2021-01-09 12:19:22 UTC; 6s ago
 Main PID: 23630 (sh)
    Tasks: 15 (limit: 4915)
   CGroup: /system.slice/bbb-rap-resque-worker.service
      ├─23630 /bin/sh -c /usr/bin/rake -f ../Rakefile resque:workers >> /var/log/bigbluebutton/bbb-rap-worker.log
      ├─23631 /usr/bin/ruby /usr/bin/rake -f ../Rakefile resque:workers
      ├─23650 resque-2.0.0: Waiting for rap:archive,rap:publish,rap:process,rap:sanity,rap:captions
      ├─23651 resque-2.0.0: Waiting for rap:archive,rap:publish,rap:process,rap:sanity,rap:captions
      └─23652 resque-2.0.0: Waiting for rap:archive,rap:publish,rap:process,rap:sanity,rap:captions

systemctl status bbb-rap-resque-worker.service shows three resque workers ready to process upto three recordings in parallel.
```


# Create dialplan (allow SIP call in)
```
root@vcr000:/opt/freeswitch/conf/dialplan/public# cat my_provider.xml 
<extension name="from_my_provider">
 <condition field="destination_number" expression="^31152010935">
   <action application="answer"/>
   <action application="sleep" data="500"/>
   <action application="play_and_get_digits" data="5 5 3 7000 # conference/bbb-surfcloud.wav ivr/ivr-that_was_an_invalid_entry.wav pin \d+"/>
   <action application="transfer" data="SEND_TO_CONFERENCE XML public"/>
 </condition>
</extension>
<extension name="check_if_conference_active">
 <condition field="${conference ${pin} list}" expression="/sofia/g" />
 <condition field="destination_number" expression="^SEND_TO_CONFERENCE$">
   <action application="set" data="bbb_authorized=true"/>
   <action application="transfer" data="${pin} XML default"/>
 </condition>
</extension>
```

Store wav files
```
root@vcr000:/opt/freeswitch/conf/dialplan/public# find / | grep bbb-surfcloud.wav
/opt/freeswitch/share/freeswitch/sounds/en/us/callie/conference/48000/bbb-surfcloud.wav
/opt/freeswitch/share/freeswitch/sounds/en/us/callie/conference/16000/bbb-surfcloud.wav
/opt/freeswitch/share/freeswitch/sounds/en/us/callie/conference/32000/bbb-surfcloud.wav
/opt/freeswitch/share/freeswitch/sounds/en/us/callie/conference/8000/bbb-surfcloud.wav
```

# Create record cleanup script
```
mkdir -p /opt/bbb-tools
```
place del_pres.py in there; The file has to be updated for bbb2.3: `m = re.search('(?P<URL>https:.*/bigbluebutton/)', line)`

## Place cronjob
```
vcr001.bbb.tbm.tudelft.nl bigbluebutton # cat /etc/cron.d/bbb-rec-del 
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

*/5 * * * * root /opt/bbb-tools/del_pres.py 
```

# Video Processing
Use: https://github.com/tilmanmoser/bbb-video-download
```
cd /opt
git clone https://github.com/tilmanmoser/bbb-video-download.git
cd bbb-video-download
```
create tmp folder:
```
vcr001.bbb.tbm.tudelft.nl bbb-video-download # mkdir /opt/bbb-video-download/tmp
```
create tmp mounti (adjust for BBB gid!):
```
tmpfs /opt/bbb-video-download/tmp/ tmpfs rw,nosuid,noatime,uid=998,gid=998,size=16G,mode=0755   0    0
mount -a
```

Install docker-compose from upstream:
```
curl -L "https://github.com/docker/compose/releases/download/1.28.6/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```

Note to use updated template for installation (see: https://github.com/tilmanmoser/bbb-video-download/pull/60/files)

# munin
```
apt-get install munin-node munin-plugins-extra
```

## node-load
The bottleneck of BBB is a single node process that runs single threaded. Hence, we monitor per-node-process CPU utilization.
```
apt-get install python3-psutil
```
place `/etc/munin/plugins/node_load`

```
#!/usr/bin/env python3 
import psutil
import sys
import os
import subprocess

# get all node processes
pid_idx = {}

def get_idx(pidname, pid):
	global pid_idx
	if pidname in pid_idx:
		if not pidname+str(pid) in pid_idx:
			ret = max(pid_idx[pidname])
			pid_idx[pidname].append(ret+1)
			pid_idx[pidname+str(pid)] = ret
		else:
			ret = pid_idx[pidname+str(pid)]
	else:
		ret = 1
		pid_idx[pidname] = [2]
		pid_idx[pidname+str(pid)] = 1
	return ret

def get_node_load(config='5'):
	procs = {}
	pids = []
	for proc in psutil.process_iter():
		try:
			# Get process name & pid from process object.
			#pInfoDict = proc.as_dict(attrs=['pid', 'name', 'cpu_percent'])
			pInfoDict = proc.as_dict(attrs=['pid','cmdline'])
			if 'node' in ' '.join(pInfoDict['cmdline']) and '.js' in ' '.join(pInfoDict['cmdline']):
				pids.append(pInfoDict)
		except:
			pass

	process = subprocess.Popen(['/usr/bin/top','-b','-n',config], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = process.communicate()
	top = stdout.decode('UTF-8').strip().split('\n')
	for proc in pids:
		for t in top:
			t_items = t.split()
			if t_items:
				if t_items[0] == str(proc['pid']):
					pidname = proc['cmdline'][-1].split('/')[-1]
					pidname = pidname+'-'+str(get_idx(pidname, proc['pid']))
					try:
						procs[pidname] = t_items[-4]
					except:
						procs[pidname] = '0.0'
	return procs

def print_config():
	print('config bbb-node-load')
	print('graph_title CPU Load per Node Process')
	print('graph_args --base 1000 -l 0')
	print('graph_vlabel CPU Load')
	print('graph_category bigbluebutton')
	print('graph_scale no')
	data = get_node_load(config='1')
	keys = list(data.keys())
	keys.sort()
	for i in keys:
		print(i.replace('.','-').replace('=','-')+'.label '+i)
		print(i.replace('.','-').replace('=','-')+'.draw AREASTACK')

def print_data():
	data = get_node_load()
	keys = list(data.keys())
	keys.sort()
	for i in keys:
		print(i.replace('.','-').replace('=','-')+'.value '+str(data[i]))

if len(sys.argv) > 1:
	if sys.argv[1] == 'config':
		print_config()
else:
	print_data()
```


# backup

```
mkdir /srv/backup
```
add backup to /etc/fstab
```
10.23.42.1:/srv/nfsroot/backups/vcr000.bbb.tbm.tudelft.nl   /srv/backup nfs defaults 0 0
```

Place daily cron job
```
#!/bin/bash

rsync -a /etc /srv/backup
rsync -a /opt /srv/backup

touch /srv/backup/DONE
```

into /etc/cron.daily/backup

```
chmod +x backup
```

# Disable auto-updates for BBB

Disable automatic upgrades for ferm and BBB in `/etc/apt/apt.conf.d/50unattended-upgrades`:

```
Unattended-Upgrade::Package-Blacklist {
        "ferm";
        "bigbluebutton";
        "bbb-apps";
        "bbb-apps-akka";
        "bbb-apps-screenshare";
        "bbb-apps-sip";
        "bbb-apps-video";
        "bbb-apps-video-broadcast";
        "bbb-client";
        "bbb-config";
        "bbb-etherpad";
        "bbb-freeswitch-core";
        "bbb-freeswitch-sounds";
        "bbb-fsesl-akka";
        "bbb-html5";
        "bbb-mkclean";
        "bbb-playback-presentation";
        "bbb-record-core";
        "bbb-red5";
        "bbb-transcode-akka";
        "bbb-web";
        "bbb-webrtc-sfu";
        "bigbluebutton";
        "ffmpeg";
        "libavcodec58:amd64";
        "libavdevice58:amd64";
        "libavfilter7:amd64";
        "libavformat58:amd64";
        "libavresample4:amd64";
        "libavutil56:amd64";
        "libopusenc0";
        "libpostproc55:amd64";
        "libswresample3:amd64";
        "libswscale5:amd64";
```

# Log Retention
In /etc/cron.daily/bigbluebutton:
```
#
# How N days back to keep files
#
history=5
unrecorded_days=7
published_days=7
log_history=7
```

# CPU Freq scheduling

```
apt-get install cpufrequtils
```

Switch to performance due to audio distortion from freq scaling while audio is being played.
```
systemctl disable ondemand.service

~ # cat /etc/default/cpufrequtils 
### Hetzner Online GmbH - installimage
# cpu frequency scaling
ENABLE="true"
GOVERNOR="performance"
MAX_SPEED="0"
MIN_SPEED="0"

```


