# base system
The base system can be updated using `apt-get update; apt-get dist-upgrade`; 
You have to look out for bbb components or ferm being upgraded

# BigBlueButton

## Announcing Maintenance
If you have to update the systems, [check first if there are running rooms](https://bbb.tbm.tudelft.nl/b/admins/rooms), you find maintenance announce scripts for vcr000/vcr001 in /opt/maintenance on frontend. These scripts spawn a container that joins each active room, posts 'This server is going down for maintenance in 5min' to the room, and then leaves again.

## Upgrade

Before upgrading, read the newest release notes at: https://github.com/bigbluebutton/bigbluebutton/releases 
Also use `bbb-record --list` to verify that there are _no_ recordings still processing. If there are still recordings, please let these process first.

Check that the node is empty using `/opt/monitoring/bbb_status.py`

Disable a node in the LB before upgrading it and re-enable it afterwards (and test that sessions can be started etc.)

Use the dev setup on frontend (/srv/greenlight-surf-dev) to test upgraded nodes (adjust .env to directly point to a node); Recordings do not become visible there, though, as they are processed by scalelite.

1. `apt-get update; apt-get dist-upgrade`
2. `bbb-conf --setip $(hostname)`, so, e.g., bbb-conf --setip vcr000.bbb.tbm.tudelft.nl (but rather use the shell expand)
3. Afterwards check/adjust configuration for bbb: For each file in `/etc/bigbluebutton/bbb-conf/permanent_configuration/`, vimdiff it against the same-name file in `/etc/bigbluebutton/bbb-conf/pre_update_backup/` (Files as changed by the update.) This is especially important for `settings.yml`!
4. Do another `bbb-conf --restart`
5. Check hosts individually using dev.bbb.tbm.tudelft.nl

Note: If you are doing maintenance in off-hours, you can also consider upgrading both nodes at the same time; We don't have an SLA after all, and when there are no lectures... well.


# bbb-video-download
Check https://github.com/tilmanmoser/bbb-video-download if there are updates. Then: 

```
cd /opt/bbb-video-download
git pull
chown -Rv bigbluebutton: .
docker app build
```

# reboot
After a reboot it can happen that no rooms can be started. Restart bbb then with `bbb-conf --restart`
