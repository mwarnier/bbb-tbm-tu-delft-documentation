# Upgrade of vcrs to BBB 2.4

Resources:
- BBB documentation for changes: https://docs.bigbluebutton.org/2.4/new.html
- BBB 2.4 release notes: https://github.com/bigbluebutton/bigbluebutton/releases

## Remove host to be upgraded from rotation

To remove the host to be upgraded from the rotation, login to lb.bbb and execute:
```
# docker exec -it scalelite-api /bin/sh
```
as root.

Thereafter, isse the following commands in the spawned shell in the container.

1. To get the ID of the node you want to disable: `~ $ ./bin/rake servers`
2. To disable a specific server: `./bin/rake servers:disable[b9f62e9c-6da2-4dee-85aa-032eed021907]` (example ID for vcr001, replace as appropriate.)
3. To verify that the change was successful: `~ $ ./bin/rake status`; This should show 'STATE' as disabled for the host you just disabled.

## Schedule downtime for node
Log in to https://mgmt.bbb.tbm.tudelft.nl/nagios4/ and schedule a downtime for the node and all services on the node. 

## Create full disk backup.
Before major changes, it is wise to draw a full disk backup of the system, also to be able to roll back in case of issues arising.

### Enable rescue system
The Hetzner rescue system is a live linux image, into which we can boot our servers.
To enable the recue system, log into the hetzner.de _Robot_ interface. There, go to:

1. Server
2. Select the server you want to move to the rescue system
3. Click 'Rescue` (between Reset and Linux)
4. Select `Linux` as OS, 64bit as the Architecture, and your Public Key for logging in via SSH.
5. Click `Activate rescue system`

### Reboot the host
Next, reboot the host by logging in, elevating to root, and issuing `/sbin/reboot`. After several minutes, the host should be reachable via SSH again.

### Logging into the rescue system
After the system rebooted, you can login via ssh. Keep in mind that the SSH key of the host will have changed (temporarily), as the rescue system generates a new one at boot.

SSH into the host.

### Creating the backup
We will now use pv, openssl, and netcat to securely transfer a disk-image between the node in the rescue system and lb, which also serves as staging area for backups. Please make sure to run all commands below in a screen session on the corresponding hosts. Creating the backup will take around 40 minutes.

1. Login to lb.bbb.tbm.tudelft.nl, become root, and chdir to `/srv/nfsroot/backups/images`
2. Create a symmetric key for use with openssl. A one-time 25 char string is sufficient.
3. On LB, run (adjusting the hostname based on the source!):
```
lb.bbb.tbm.tudelft.nl images # nc -l -p 8443 | openssl enc -d -aes256 -pbkdf2 -k '<SECRET KEY>' -out -| pv > ./vcr001.bbb.tbm.tudelft.nl-18.04.img
```
4. Next, on the rescue system, run:
```
root@rescue ~ # pv /dev/sda | openssl enc -e -aes256 -pbkdf2 -k '<SECRET KEY>' -out - | nc lb.bbb.tbm.tudelft.nl 8443
```

As soon as all data is transferred (~240GB), reboot the rescue system.

## Start BBB upgrade
After a successful backup, we can initiate the upgrade.

### Disable after-upgrade automation
First, we have to disable several in-version upgrade automation features. For that, comment the following lines in `/etc/bigbluebutton/bbb-conf/apply-config.sh`:

```
13
17
21
25
29
33
37
43
46
```

Afterwards, run `etckeeper commit` to commit the changes.

### Running the upgrade
You can initiate the upgrade by running; The TURN SECRET is index 0 in `/etc/bigbluebutton/turn-stun-servers.xml`:

WARNING: DO NOT USE THE COMMAND LINE SWITCHES `-w` (install firewall) and `-a` (install API demo) even though the documentation recommends that!

```
wget -qO- https://ubuntu.bigbluebutton.org/bbb-install.sh | bash -s -- -v bionic-240 -s `hostname` -e bbb-ess-ict-tbm@tudelft.nl -c turn.bbb.tbm.tudelft.nl:<TURN SECRET>
```

### Clear freeswitch log

After the upgrade, the freeswitch log will be full of (update related) errors. Clear that:

```
echo ""> /opt/freeswitch/log/freeswitch.log
```

### Disable Learning Dashboard
The BBB learning Dashboard is a walking privacy violation without a lot of consent work. It should be disabled.
For that, add `learningDashboardEnabled=false` to `/etc/bigbluebutton/bbb-web.properties` and restart BBB using:
```
bbb-conf --restart
```

### Update archive.rb
Replace `/etc/bigbluebutton/bbb-conf/permanent_configuration/archive.rb` with `bbb-tbm-tudelft-nl-documentation/config/vcr/archive.rb` from the documentation repository.

### Install TU Delft Backgrounds
Copy `bbb-tbm-tudelft-nl-documentation/config/vcr/2.4backgrounds/` into `/etc/bigbluebutton/bbb-conf/permanent_configuration/`

Make the following change to `/etc/bigbluebutton/bbb-conf/apply-config.sh`:
```
+ 48 echo "- Installing virtual backgrounds"                                         
+ 49 cp -r /etc/bigbluebutton/bbb-conf/permanent_configuration/2.4backgrounds/* /usr/share/meteor/bundle/programs/web.browser.legacy/app/resources/images/virtual-backgrounds/
+ 50 cp -r /etc/bigbluebutton/bbb-conf/permanent_configuration/2.4backgrounds/* /usr/share/meteor/bundle/programs/web.browser/app/resources/images/virtual-backgrounds/
  51                                                                                 
  52 echo "Conf-update successful"                                                   
  53                                                                                 
  54 etckeeper commit "post bbb restart commit"     
```

### Update settings.yml
settings.yml changed significantly. For this upgrade, copy `bbb-tbm-tudelft-nl-documentation/config/vcr/settings2.4.yml` to `/etc/bigbluebutton/bbb-conf/permanent_configuration/settings.yml`.
You must then make the following changes:
- There are two occurences of hostnames (vcr00X) in the file. These have to be adjusted to the node you are executing the upgrade on!
- There is an etherpad key under private.etherpad.apikey. This must be change to correspond to the contents of `/usr/share/etherpad-lite/APIKEY.txt`.
Note that the settings2.4.yml file is for BBB 2.4.2. If the minor version increased since the writing of this document, please update settings2.4.yml further after the adjustments above following the standard update procedure for BBB. 

### Update language files
Retrieve https://raw.githubusercontent.com/ichdasich/bbb-de-diverse-language/main/de_changes.json and put it into `/etc/bigbluebutton/bbb-conf/permanent_configuration/de_changes.json`

### Reactivate upgrade automation
Finally, reactivate the upgrade automation for BBB, by un-commenting the following lines in `/etc/bigbluebutton/bbb-conf/apply-config.sh`:

```
13
17
21
25
29
33
37
43
46
```

### Restore nginx configuration
The upgrade reverted back to Let's Encrypt certificates. To roll this back, place the following contents in `/etc/nginx/sites-available/bigbluebutton` and restart nginx thereafter:

NOTE: Adjust hostnames for the one you are working on!

```
server_tokens off;

server {
  listen 80;
  listen [::]:80;
  server_name vcr001.bbb.tbm.tudelft.nl;
  
  return 301 https://$server_name$request_uri; #redirect HTTP to HTTPS

}
server {
  listen 443 ssl http2;
  listen [::]:443 ssl http2;
  server_name vcr001.bbb.tbm.tudelft.nl;

    ssl_certificate /etc/ssl/vcr001.bbb.tbm.tudelft.nl.chain;
    ssl_certificate_key /etc/ssl/private/vcr001.bbb.tbm.tudelft.nl.key;

    ssl_session_cache shared:SSL:10m;
    ssl_session_timeout 10m;
    ssl_protocols TLSv1.2;
    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
    ssl_prefer_server_ciphers on;
    ssl_dhparam /etc/nginx/ssl/dhp-4096.pem;
    
    # HSTS (comment out to enable)
    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;

  #access_log  /var/log/nginx/bigbluebutton.access.log;

  # BigBlueButton landing page.
  location / {
    root   /var/www/bigbluebutton-default;
    add_header Permissions-Policy interest-cohort=();
    index  index.html index.htm;
    expires 1m;
  }

  # Include specific rules for record and playback
  include /etc/bigbluebutton/nginx/*.nginx;
}
```

Thereafter, issue:
```
service nginx restart
```

### Restart BBB
To finish the upgrade, run:
```
bbb-conf --restart
```

## Change dev.bbb.tbm.tudelft.nl to point at the node-under-change directly
To be able to individually test changes to a vcr, we can point dev.bbb.tbm.tudelft.nl only at that vcr. To do that:
1. log into frontend. 
2. There, chdir to `/srv/greenlight-surf-dev`
3. Edit ./.env; Uncomment the host/secret line you want to use (loadbalancer, vcr000, vcr001)
4. Restart the dev frontend by running `docker-compose down; docker-compose up -d`

## Execute pre-prod tests
Using the dev system, you can now start rooms on the updated node. Please test, at least, the following functionalities:

### Phone Dial-In:
- Create a room
- Use the phone call-in and see if you can hear yourself

### Recordings and recording downloadable video creation
- Create a room
- Start a recording
- Speak
- share your camera
- share your screen
- dial in via phone and speak
- draw on the whiteboard 
- check that the recording is processed by the loadbalancer
- manually download the aggregate video file/stand-alone-archive via ssh, and check if the recording is complete.

## Re-add host to live

Finally, we have to re-activate the host, which also allows us to conduct streaming tests. For this, issue the following command on lb.bbb:
```
docker exec -it scalelite-api ./bin/rake servers:enable[b9f62e9c-6da2-4dee-85aa-032eed021907]
```

Again, you will have to re-use the ID you used before to disable the node.

## Update streaming container on frontend.
Log into frontend. as root and issue:
```
docker image pull aauzid/bigbluebutton-livestreaming
```
To update the live-streaming container to the latest version, which is compatible with BBB 2.4.

Note: At the moment the official docker image does not support BBB 2.4 yet. Hence, an updated image has been build from the repository.
The cloned repo is in `/srv/streaming_debug/BigBlueButton-liveStreaming`. The image can be rebuild by:
```
cd /srv/streaming_debug
docker-compose build
```
as root.

### Streaming
- Manually start a streaming container for this node after creating a room
  + A sample docker-compose.yml file is in `/srv/streaming_debug/`
  + Information necessary for a room (currently configured for my home room) can be found in the sqlite3 db of the dev instance (`/srv/greenlight-surf-dev/db/production/production.sqlite3`)
  + Information for the host (bbb url and secret) are in the .env of the dev instance
  + The room must have been started before streaming can take place
