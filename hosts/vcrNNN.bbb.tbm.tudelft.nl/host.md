# vcr000.bbb.tbm.tudelft.nl
```
IPv4: 168.119.67.118
IPv6: 2a01:4f8:242:1549::2
IPv4 Backend (vid 4000): 10.23.42.100
Location: Falkenstein, DE
Purpose: BBB worker
Services: bbb 2.3 stack
OS: Ubuntu 18.04; DO NOT UPGRADE! Ubuntu 18.04 needed for BBB 2.3.
Comment: Only 240GB SSD / ; No RAID. 
```

# vcr001.bbb.tbm.tudelft.nl
```
IPv4: 168.119.67.117
IPv6: 2a01:4f8:242:154a::2
IPv4 Backend (vid 4000): 10.23.42.101
Location: Falkenstein, DE
Purpose: BBB worker
Services bbb 2.2 stack
OS: Ubuntu 18.04; DO NOT UPGRADE! Ubuntu 16.04 needed for BBB 2.3.
Comment: Only 240GB SSD / ; No RAID. 
```
