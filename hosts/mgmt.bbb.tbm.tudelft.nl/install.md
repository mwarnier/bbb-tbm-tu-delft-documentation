# munin
```
apt-get install munin
apt-get install apache2 libcgi-fast-perl libapache2-mod-fcgid certbot python3-certbot-apache
certbot --apache -d mgmt.bbb.tbm.tudelft.nl
```

add munin config (hosts)  

add apache config to ssl vhost:  
```
Alias /munin /var/cache/munin/www
<Directory /var/cache/munin/www>
         Require all granted
         Options FollowSymLinks SymLinksIfOwnerMatch
        Options None
</Directory>

ScriptAlias /munin-cgi/munin-cgi-graph /usr/lib/munin/cgi/munin-cgi-graph
<Location /munin-cgi/munin-cgi-graph>
 Require all granted
 Options FollowSymLinks SymLinksIfOwnerMatch

        <IfModule mod_fcgid.c>
            SetHandler fcgid-script
        </IfModule>
        <IfModule !mod_fcgid.c>
            SetHandler cgi-script
        </IfModule>
</Location>
```

Restart apache

# Nagios
```
apt-get install libapache2-mod-php7.4 nagios4 monitoring-plugins-common
a2enmod rewrite headers cgi auth_digest authz_groupfile
```

remove restriction to localhost/1918 in `/etc/apache2/conf-enabled/nagios4-cgi.conf`

Enable auth in `/etc/nagios4/cgi.cfg`

Apply basic auth to all; remove `grant all`

# postfix
We need a mailserver to send notifications by mail
```
apt-get install libsasl2-modules postfix mailutils
```
Create `/etc/postfix/sasl_passwd`

```
[mail.isp.example]:587 username:password
postmap /etc/postfix/sasl_passwd
sudo chown root:root /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.db
sudo chmod 0600 /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.db
```


Place /etc/postfix/main.cf

```
relayhost = [smtp-a.tudelft.nl]:465
# enable SASL authentication
smtp_sasl_auth_enable = yes
# disallow methods that allow anonymous authentication.
smtp_sasl_security_options = noanonymous
# where to find sasl_passwd
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
# Enable STARTTLS encryption
smtp_use_tls = yes
smtp_tls_wrappermode = yes
smtp_tls_security_level = encrypt
```

On this host, we allow inbound connections on port 25, as we use it to monitor mail-sending using a custom receipt script (pipe delivery) and a passive nagios check.

