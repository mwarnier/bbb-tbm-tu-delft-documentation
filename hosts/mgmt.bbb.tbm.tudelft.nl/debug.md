# General Debug


# Monitoring Debug
This section lists the monitoring scripts running for this host and details their debug pathways.

## APT update status
### Description
Goes to CRITICAL for security updates, and to WARNING for feature updates; 

### Debug and Resolution
Update the host.

## Check TLS Certificate Status
### Desciption
Checks certificate status for mgtm.bbb.tbm.tudelft.nl Turns to WARNING 28 days before expirery, and to CRITICAL 14 days before expirery.

### Debug and Resolution
Request a new certificate from TU Delft central IT.

## Check for email relaying via TU Delft

### Description
This check is passive. It is updated when the monitoring system receives an email relayed via TU Delft's mail servers (send via 'Send monitoring mails for check_mail_delivery' below).

### Debug and Resolution
If this fails, inspect the mailq on the host and investigate whether manual relaying via smtp-a.tudelft.nl is possible.

## Check forward DNS

### Descirption 
This check ensures that mgmt.bbb.tbm.tudelft.nl points to the host's IPv4 address.

### Debug and Resolution
Verify against the NS of tudelft.nl that they do return the correct names. If not, contact central to get the names fixed.

## Check network interface bandwidth utilization

### Descirption
Checks utilization of the network interface. Warning if more than 80%, critical if more than 90% bandwidth are used.

### Debug and Resolution
Except for the workers, this should not happen. Might occur occasionally. If this happens on one of the workers due to a large meeting, cross check meeting sizes at that time in munin and upgrade to a 10gE interface for all workers.

## Check reverse DNS

### Description
Checks if the reverse DNS of the host matches its name.

### Debug and Resolution
Check that the correct name is being set in the Hetzner interface.

## HTTPS

### Description
Checks if the webserver is running on port 443.

### Debug and Resolution
Check if apache is running, and if not, investigate why in the logs:

```
systemctl status apache
jornalctl -xn
```

## Root Partition

### Description
Checks utilization of the root partition. Warning if less than 20% are free, critical, if less than 10% are free.

### Debug and Resolution
This can occur if monitoring consumes too much space for historical data. Technically unlikely; Enlarge the disk.

## SSH
### Description
Checks availability of SSH.

### Debug and Resolution
Use the cloud remote console from hetzner and check VTY for errors.

## Send monitoring mails for check_mail_delivery

### Description
Always OK; Sends an email for the passive mail-sending check.
