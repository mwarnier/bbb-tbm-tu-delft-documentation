# mgmt.bbb.tbm.tudelft.nl
```
IPv4: 95.217.131.128
IPv6: 2a01:4f9:c010:6f78::1
IPv4 Backend (vid 4000): none
Location: Virtual, Helsinki, FI
Purpose: Historic and realtime monitoring
Services: Munin, Icinga
OS: Ubuntu 20.04
Comment: Virtual Machine
```
